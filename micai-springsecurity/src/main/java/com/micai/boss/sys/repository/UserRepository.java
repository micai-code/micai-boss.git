/**
 * Copyright 2020. javaymw.com Studio All Right Reserved
 * <p>
 * Create on 2020-06-05 22:03
 * Created by zhaoxinguo
 * Version 2.0.0
 */
package com.micai.boss.sys.repository;

import com.micai.boss.sys.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @description: TODO
 * @author zhaoxinguo
 * @date 2020/6/5 22:03
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    User findUserByUsername(String username);
}
