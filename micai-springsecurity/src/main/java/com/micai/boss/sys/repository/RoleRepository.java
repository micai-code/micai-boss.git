/**
 * Copyright 2020. javaymw.com Studio All Right Reserved
 * <p>
 * Create on 2020-06-07 11:48
 * Created by zhaoxinguo
 * Version 2.0.0
 */
package com.micai.boss.sys.repository;

import com.micai.boss.sys.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @description: TODO
 * @author zhaoxinguo
 * @date 2020/6/7 11:48
 */
public interface RoleRepository extends JpaRepository<Role, Integer> {


}
