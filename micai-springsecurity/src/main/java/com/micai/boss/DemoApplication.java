package com.micai.boss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
//		System.out.println(new BCryptPasswordEncoder().encode("123456"));
		SpringApplication.run(DemoApplication.class, args);
	}

}
