/**
 * Copyright 2020. javaymw.com Studio All Right Reserved
 * <p>
 * Create on 2020-06-06 15:03
 * Created by zhaoxinguo
 * Version 2.0.0
 */
package com.micai.boss.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * @description: Spring Session配置类
 * @author zhaoxinguo
 * @date 2020/6/6 15:03
 */
@Configuration
@EnableRedisHttpSession
public class SessionConfig {


}
