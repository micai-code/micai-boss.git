package com.micai.boss.order;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/2/24 9:40
 */
@SpringBootApplication
// 解决扫描redis包路径
@ComponentScan(basePackages = {"com.micai.boss.order.receiver"})
public class OrderConsumerApplication implements CommandLineRunner {

    private  final Logger log = LoggerFactory.getLogger(OrderConsumerApplication.class);

    public static void main(String [] args) throws InterruptedException {
        SpringApplication.run(OrderConsumerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("订单----->>服务消费者启动完毕------>>启动完毕");
    }
}

