package com.micai.boss.order.receiver;

import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/2/28 17:25
 */
@Component
@RabbitListener(queues = "userQueue")
public class UserReceiver {

    private static Logger logger = LoggerFactory.getLogger(UserReceiver.class);

    @RabbitHandler
    public void process(String message) {
        JSONObject jsonObject = JSONObject.fromObject(message);
        logger.info("接收消息: {}", jsonObject.toString());
    }

}
