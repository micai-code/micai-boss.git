package com.micai.boss.rabbitmq.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/2/28 17:28
 */
@Configuration
public class QueueConfig {

    @Bean
    public Queue userQueue() {
        return new Queue("userQueue");
    }
}
