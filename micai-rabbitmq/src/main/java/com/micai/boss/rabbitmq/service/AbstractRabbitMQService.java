package com.micai.boss.rabbitmq.service;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/2/28 17:13
 */
@Component
public abstract class AbstractRabbitMQService {

    @Autowired
    private AmqpTemplate amqpTemplate;

    public void convertAndSend(Object message) {
        amqpTemplate.convertAndSend("userQueue", message);
    }

}
