package com.micai.boss.weixin.constant;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/19 10:54
 */
public class WeiXinConstant {

    //微信缓存数据Key
    public static final String WX_CACHE_TOKEN_KEY = "sdx_wx_token_";
    public static final String WX_CACHE_TICKET_KEY = "sdx_wx_ticket_";

    public static final String WX_CACHE_USER_INFO_KEY = "sdx_wx_user_info_";
    public static final String WX_CACHE_SCOPE_TOKEN_KEY = "sdx_wx_scope_token_";

}
