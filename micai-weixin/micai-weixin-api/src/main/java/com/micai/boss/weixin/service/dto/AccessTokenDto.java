package com.micai.boss.weixin.service.dto;

import java.io.Serializable;

/**
 * 描述：微信通用接口凭证
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/19 10:56
 */
public class AccessTokenDto implements Serializable {

    // 获取到的凭证
    private String token;
    // 凭证有效时间，单位：秒
    private int expiresIn;
    // 最后更新时间
    private Long timestamp;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(int expiresIn) {
        this.expiresIn = expiresIn;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}
