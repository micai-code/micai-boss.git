package com.micai.boss.weixin.service.dto;

import java.io.Serializable;

/**
 * 描述：微信凭证对象
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/19 10:56
 */
public class TicketDto implements Serializable {

    // 获取到的凭证
    private String ticket;
    // 凭证有效时间，单位：秒
    private int expiresIn;
    private Long timestamp;

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(int expiresIn) {
        this.expiresIn = expiresIn;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "ticket='" + ticket + '\'' +
                ", expiresIn=" + expiresIn +
                '}';
    }
}
