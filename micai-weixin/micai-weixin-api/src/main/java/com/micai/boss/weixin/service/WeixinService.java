package com.micai.boss.weixin.service;

import com.micai.boss.base.result.BaseResult;
import com.micai.boss.weixin.service.dto.AccessTokenDto;
import com.micai.boss.weixin.service.dto.SnsAuthDto;
import com.micai.boss.weixin.service.dto.TicketDto;
import com.micai.boss.weixin.service.dto.WeixinUserInfoDto;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/19 10:59
 */
public interface WeixinService {

    /**
     * 获取普通token
     * @return
     */
    public BaseResult<AccessTokenDto> getAccessToken();

    /**
     * 获取ticket
     * @return
     */
    public BaseResult<TicketDto> getTicket();

    /**
     * 获取网页token
     * @param code
     * @return
     */
    public BaseResult<SnsAuthDto> getScopeAccessToken(String code);

    /**
     * 获取用户信息
     * @param openId
     * @return
     */
    public BaseResult<WeixinUserInfoDto> getUserInfo(String openId);
}
