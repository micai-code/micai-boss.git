package com.micai.boss.weixin.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.micai.boss.base.constant.ErrorCodeEnum;
import com.micai.boss.base.exception.ServiceException;
import com.micai.boss.base.result.BaseResult;
import com.micai.boss.base.utils.BeanCopierUtils;
import com.micai.boss.weixin.entity.AccessToken;
import com.micai.boss.weixin.entity.Ticket;
import com.micai.boss.weixin.repository.WeiXinRepository;
import com.micai.boss.weixin.repository.WeiXinUserRepository;
import com.micai.boss.weixin.service.WeixinService;
import com.micai.boss.weixin.service.dto.AccessTokenDto;
import com.micai.boss.weixin.service.dto.SnsAuthDto;
import com.micai.boss.weixin.service.dto.TicketDto;
import com.micai.boss.weixin.service.dto.WeixinUserInfoDto;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/19 11:07
 */
@Service(version = "1.0.0", protocol = {"dubbo", "feign"}, timeout = 10000)
public class WeixinServiceImpl implements WeixinService {

    @Autowired
    private WeiXinRepository weiXinRepository;

    @Autowired
    private WeiXinUserRepository weiXinUserRepository;

    /**
     * 获取普通token
     * @return
     */
    @Override
    public BaseResult<AccessTokenDto> getAccessToken() {
        BaseResult<AccessTokenDto> baseResult = null;
        try {
            AccessToken accessToken = weiXinRepository.getAccessToken();
            if(null != accessToken){
                baseResult = new BaseResult<>(BeanCopierUtils.convert(accessToken, AccessTokenDto.class));
            }else {
                baseResult = new BaseResult<>(null);
            }
        } catch (ServiceException se) {
            baseResult = new BaseResult<>(null, se.getCode(), se.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            baseResult = new BaseResult<>(null, ErrorCodeEnum.系统异常.getCode(), ErrorCodeEnum.系统异常.getMessage());
        }
        return baseResult;
    }

    /**
     * 获取ticket
     * @return
     */
    @Override
    public BaseResult<TicketDto> getTicket() {
        BaseResult<TicketDto> baseResult = null;
        try{
            //S1.3 获取Ticket
            Ticket ticket = weiXinRepository.getTicket();
            if (null != ticket){
                baseResult = new BaseResult<>(BeanCopierUtils.convert(ticket, TicketDto.class));
            }else {
                baseResult = new BaseResult<>(null);
            }
        }catch (ServiceException se){
            baseResult = new BaseResult<>(null, se.getCode(), se.getMessage());
        }catch (Exception e){
            e.printStackTrace();
            baseResult = new BaseResult<>(null, ErrorCodeEnum.系统异常.getCode(), ErrorCodeEnum.系统异常.getMessage());
        }
        return baseResult;
    }

    /**
     * 授权获取网页token
     * @param code
     * @return
     */
    @Override
    public BaseResult<SnsAuthDto> getScopeAccessToken(String code) {
        BaseResult<SnsAuthDto> baseResult = null;
        try {
            baseResult = new BaseResult<>(weiXinRepository.getScopeAccessToken(code));
        }catch (ServiceException se){
            baseResult = new BaseResult<>(null, se.getCode(), se.getMessage());
        }catch (Exception e){
            e.printStackTrace();
            baseResult = new BaseResult<>(null, ErrorCodeEnum.系统异常.getCode(), ErrorCodeEnum.系统异常.getMessage());
        }
        return baseResult;
    }

    /**
     * 获取用户信息
     * @param openId
     * @return
     */
    @Override
    public BaseResult<WeixinUserInfoDto> getUserInfo(String openId) {
        BaseResult<WeixinUserInfoDto> baseResult = null;
        try {
            baseResult = new BaseResult<>(weiXinUserRepository.getUserInfo(openId));
        }catch (ServiceException se){
            baseResult = new BaseResult<>(null, se.getCode(), se.getMessage());
        }catch (Exception e){
            e.printStackTrace();
            baseResult = new BaseResult<>(null, ErrorCodeEnum.系统异常.getCode(), ErrorCodeEnum.系统异常.getMessage());
        }
        return baseResult;
    }
}
