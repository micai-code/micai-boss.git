package com.micai.boss.weixin.api;

import com.alibaba.fastjson.JSONObject;
import com.micai.boss.base.result.BaseResult;
import com.micai.boss.weixin.service.WeixinService;
import com.micai.boss.weixin.service.dto.AccessTokenDto;
import com.micai.boss.weixin.service.dto.SnsAuthDto;
import com.micai.boss.weixin.service.dto.TicketDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Auther: zhaoxinguo
 * @Date: 2018/9/14 16:12
 * @Description:
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class WeixinServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(WeixinServiceTest.class);

    @Autowired
    private WeixinService weixinService;

    /**
     * 获取普通token
     */
    @Test
    public void getAccessTokenTest() {
        BaseResult<AccessTokenDto> accessToken = weixinService.getAccessToken();
        LOGGER.info("getAccessTokenTest accessToken: {}", JSONObject.toJSON(accessToken));
    }

    /**
     * 获取ticket
     */
    @Test
    public void getTicketTest() {
        BaseResult<TicketDto> ticket = weixinService.getTicket();
        LOGGER.info("getTicketTest ticket: {}", JSONObject.toJSON(ticket));
    }

    /**
     * 获取网页token
     */
    @Test
    public void getScopeAccessTokenTest() {
        String code = "13_uoUCivR1D6EM7EOGma5b6xF_AMsyTg7ErQlGIVX2MW_t4Jc9_RQQIsEexNnohdwqjNHQ6fsPCYoTSWO5WUoVZWesE4XpNJB4RNG7HXAojgVaEa8zmcEtag9IDtQXPGhAEAGYP";
        BaseResult<SnsAuthDto> scopeAccessToken = weixinService.getScopeAccessToken(code);
        LOGGER.info("getScopeAccessTokenTest scopeAccessToken: {}", JSONObject.toJSON(scopeAccessToken));
    }
}
