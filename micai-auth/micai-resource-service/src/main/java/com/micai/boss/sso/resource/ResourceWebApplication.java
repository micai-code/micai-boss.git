package com.micai.boss.sso.resource;

import com.micai.boss.sso.resource.filter.JwtFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import java.util.Collections;

/**
 * 描述：资源服务启动类
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/5/21 11:38
 */
@SpringBootApplication
public class ResourceWebApplication extends SpringBootServletInitializer {

    @Value("${services.auth}")
    private String authService;

    @Bean
    public FilterRegistrationBean jwtFilter() {
        final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new JwtFilter());
        registrationBean.setInitParameters(Collections.singletonMap("services.auth", authService));
        /*registrationBean.addUrlPatterns("/protected-resource");*/
        registrationBean.addUrlPatterns("/protected-resource", "/logout");
        return registrationBean;
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(ResourceWebApplication.class);
    }

    public static void main(String [] args) {
        SpringApplication.run(ResourceWebApplication.class, args);
    }
}
