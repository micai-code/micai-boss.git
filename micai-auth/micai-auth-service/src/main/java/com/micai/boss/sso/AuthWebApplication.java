package com.micai.boss.sso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * 描述：认证服务启动类
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/5/21 11:12
 */
@SpringBootApplication
public class AuthWebApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(AuthWebApplication.class);
    }

    public static void main(String [] args) {
        SpringApplication.run(AuthWebApplication.class, args);
    }
}
