package com.micai.boss.utils;

import com.micai.boss.base.utils.WebServiceUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * @ClassName WebServiceUtilsTest
 * @Description TODO
 * @Author zhaoxinguo
 * @Date 2021/11/23 18:35
 * @Version 1.0
 */
public class WebServiceUtilsTest {

    public static void main(String[] args) {
        String soapUrl = "http://172.18.3.243:8080/SMSG/services/SMS";
        String loginId = "admin";
        String pwd = "123456";
        try {
            // 方法一
            /*String response = doPostSoap(soapUrl, getSoapXML(loginId, pwd), null);*/
            // 方法二
            /*String response = doPostSoap1_1(soapUrl, getSoapXML(loginId, pwd), null);*/
            // 方法三
            /*String response = doPostSoap1_2(soapUrl, getSoapXML(loginId, pwd), null);*/
            // 方法四
            String response = WebServiceUtils.httpURLConnection(soapUrl, WebServiceUtils.getSoapXML(loginId, pwd));
            System.out.println("response: " + response);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
