package com.micai.boss.utils;

import org.apache.commons.lang3.time.DateUtils;

import java.util.Date;

/**
 * @ClassName DateUtilsTest
 * @Description TODO
 * @Author zhaoxinguo
 * @Date 2021/11/23 18:29
 * @Version 1.0
 */
public class DateUtilsTest {

    public static void main(String[] args) throws Exception {
        // 计算两个日期间隔多少年、多少月、多少天
        Date startDate = DateUtils.parseDate("2001-11-01 10:22:20", "yyyy-MM-dd HH:mm:ss");
        Date endDate = new Date();
        String betweenYearMonthDay = com.micai.boss.base.utils.DateUtils.betweenYearMonthDay(startDate, endDate);
        System.out.println(betweenYearMonthDay);
    }
}
