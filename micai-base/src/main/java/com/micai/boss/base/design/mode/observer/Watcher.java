package com.micai.boss.base.design.mode.observer;

import java.util.Observable;
import java.util.Observer;

/**
 * @author zhaoxinguo on 2018/4/29.
 * @email sxdtzhaoxinguo@163.com
 * @qq 827358369
 * @phone 18611966723
 * @description 观察者类源代码
 */
public class Watcher implements Observer {

    public Watcher(Observable o) {
        o.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("状态发生改变：" + ((Watched)o).getData());
    }
}
