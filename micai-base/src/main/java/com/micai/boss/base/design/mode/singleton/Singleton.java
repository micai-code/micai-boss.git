package com.micai.boss.base.design.mode.singleton;

/**
 * 描述：线程安全的单例模式
 * <p>
 * (1) 私有化构造函数
 * (2) 定义静态的Singleton instance对象和getInstance()方法
 * (3) getInstance()方法中需要使用同步锁synchronized (Singleton.class)防止多线程同时进入造成instance被多次实例化
 * 可以看到上面在synchronized (Singleton.class)外又添加了一层if，这是为了在instance已经实例化后下次进入不必执行synchronized (Singleton.class)获取对象锁，
 * 从而提高性能。
 *
 * 描述：单例的作用
 * (1) 保持程序运行过程中该类始终只存在一个示例
 * (2) 对于new性能消耗较大的类，只实例化一次可以提高性能
 *
 * @author: 赵新国
 * @date: 2018/4/26 9:34
 */
public class Singleton {

    private static volatile Singleton instance = null;

    private Singleton() {}

    public static Singleton getInstance() {
        if (instance == null) {
            synchronized (Singleton.class) {
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }

}
