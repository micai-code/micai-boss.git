package com.micai.boss.base.design.mode.prototype;

import java.io.Serializable;

/**
 * 描述：大圣还持有一个金箍棒的实例，金箍棒类GoldRingedStaff:
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 18:05
 */
public class GoldRingedStaff implements Serializable {

    private float height = 100.0f;
    private float diameter = 10.0f;

    /**
     * 增长行为，每次调用长度和半径增加一倍
     */
    public void grow(){
        this.diameter *= 2;
        this.height *= 2;
    }

    /**
     * 缩小行为，每次调用长度和半径减少一半
     */
    public void shrink(){
        this.diameter /= 2;
        this.height /= 2;
    }

}
