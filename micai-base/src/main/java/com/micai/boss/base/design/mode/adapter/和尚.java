package com.micai.boss.base.design.mode.adapter;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/28 12:54
 */
public interface 和尚 {

    public void 吃斋();
    public void 念经();
    public void 打坐();
    public void 撞钟();
    public void 习武();
    public String getName();
}
