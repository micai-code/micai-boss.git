package com.micai.boss.base.design.mode.template.method;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/28 17:29
 */
public class ConcreteTemplate extends AbstractTemplate {

    /**
     * 基本方法的实现
     */
    @Override
    protected void abstractMethod() {
        //业务相关的代码

    }

    /**
     * 重写父类的方法
     */
    @Override
    protected void hookMethod() {
        //业务相关的代码

    }
}
