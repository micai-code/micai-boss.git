package com.micai.boss.base.design.mode.singleton;

/**
 * 描述：线程安全（双重校验模式）
 * <p>
 * 最常用的一种。
 *
 * @author: 赵新国
 * @date: 2018/4/26 10:16
 */
public class Singleton7 {

    private static volatile Singleton7 instance;

    private Singleton7() {}

    public static Singleton7 getInstance() {
        if (instance == null) {
            synchronized (Singleton7.class) {
                if (instance == null) {
                    instance = new Singleton7();
                }
            }
        }
        return instance;
    }
}
