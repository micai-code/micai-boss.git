package com.micai.boss.base.design.mode.builder;

/**
 * 描述：抽象建造者类Builder
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 15:29
 */
public interface Builder {

    public void buildPart1();

    public void buildPart2();

    public Product retrieveResult();
}
