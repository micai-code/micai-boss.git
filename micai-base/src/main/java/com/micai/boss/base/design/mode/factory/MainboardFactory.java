package com.micai.boss.base.design.mode.factory;

/**
 * 描述：主板工厂类
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 11:06
 */
public class MainboardFactory {

    public static Mainboard createMainboard(int type) {
        Mainboard mainboard = null;
        if (type == 1) {
            mainboard = new IntelMainboard(755);
        } else if (type == 2) {
            mainboard = new AmdMainboard(938);
        }
        return mainboard;
    }

}
