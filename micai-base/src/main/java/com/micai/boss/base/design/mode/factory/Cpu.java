package com.micai.boss.base.design.mode.factory;

/**
 * 描述：CPU接口
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 10:41
 */
public interface Cpu {

    public void calculate();

}
