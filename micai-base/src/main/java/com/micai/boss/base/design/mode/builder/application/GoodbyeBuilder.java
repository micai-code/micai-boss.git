package com.micai.boss.base.design.mode.builder.application;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 16:05
 */
public class GoodbyeBuilder extends Builder {

    public GoodbyeBuilder() {
        autoMessage = new GoodbyeMessage();
    }

    @Override
    public void buildSubject() {
        autoMessage.setSubject("欢送标题");
    }

    @Override
    public void buildBody() {
        autoMessage.setBody("欢送内容");
    }
}
