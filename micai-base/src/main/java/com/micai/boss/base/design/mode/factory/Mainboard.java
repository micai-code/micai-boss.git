package com.micai.boss.base.design.mode.factory;

/**
 * 描述：主板接口
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 10:46
 */
public interface Mainboard {

    public void installCPU();

}
