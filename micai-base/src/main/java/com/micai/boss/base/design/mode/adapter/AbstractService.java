package com.micai.boss.base.design.mode.adapter;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/28 12:58
 */
public interface AbstractService {

    public void serviceOperation1();

    public int serviceOperation2();

    public String serviceOperation3();
}
