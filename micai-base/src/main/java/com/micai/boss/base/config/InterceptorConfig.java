package com.micai.boss.base.config;

import com.micai.boss.base.interceptor.BaseInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/5/4 15:24
 */
@Configuration
public class InterceptorConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //这里可以添加多个拦截器
        //addPathPatterns("/**")对所有请求都拦截，但是排除了/getTableData请求的拦截
        registry.addInterceptor(new BaseInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns(
                        "/getTableData",
                        "/index",
                        "/getUserName"
                );
        super.addInterceptors(registry);
    }

}
