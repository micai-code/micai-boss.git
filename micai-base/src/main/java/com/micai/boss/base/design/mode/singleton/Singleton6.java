package com.micai.boss.base.design.mode.singleton;

/**
 * 描述：线程安全（枚举类）
 * <p>
 * 这种方式是Effective Java作者Josh Bloch 提倡的方式，它不仅能避免多线程同步问题，而且还能防止反序列化重新创建新的对象，可谓是很坚强的壁垒啊，推荐！
 *
 * JVM会保证enum不能被反射并且构造器方法只执行一次,因此该单例是线程安全的。
 *
 * @author: 赵新国
 * @date: 2018/4/26 10:14
 */
public class Singleton6 {

    private enum MyEnumSingleton {

        enumFactory;

        private Singleton6 singleton6;

        private MyEnumSingleton() {
            singleton6 = new Singleton6();
        }

        public Singleton6 getInstance() {
            return singleton6;
        }
    }

    public static Singleton6 getInstance() {
        return MyEnumSingleton.enumFactory.getInstance();
    }

}
