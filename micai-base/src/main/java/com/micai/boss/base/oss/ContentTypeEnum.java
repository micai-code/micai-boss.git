package com.micai.boss.base.oss;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/23 14:31
 */
public enum ContentTypeEnum {

    IMAGE("image", "图片"),
    PDF("pdf", "pdf文档"),
    HTML("html", "html文档"),
    TXT("txt", "txt文本");

    private String type;
    private String des;

    private ContentTypeEnum(String type, String des) {
        this.type = type;
        this.des = des;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }
}
