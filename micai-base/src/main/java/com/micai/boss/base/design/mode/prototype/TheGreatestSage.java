package com.micai.boss.base.design.mode.prototype;

import java.io.IOException;

/**
 * 描述：孙大圣本人用TheGreatestSage类代表
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 18:03
 */
public class TheGreatestSage {

    private Monkey monkey = new Monkey();

    public void change() throws IOException, ClassNotFoundException {
        //克隆大圣本尊
//        Monkey copyMonkey = (Monkey)monkey.clone();
        Monkey copyMonkey = (Monkey)monkey.deepClone();
        System.out.println("大圣本尊的生日是：" + monkey.getBirthDate());
        System.out.println("克隆的大圣的生日是：" + monkey.getBirthDate());
        System.out.println("大圣本尊跟克隆的大圣是否为同一个对象 " + (monkey == copyMonkey));
        System.out.println("大圣本尊持有的金箍棒 跟 克隆的大圣持有的金箍棒是否为同一个对象？ " + (monkey.getStaff() == copyMonkey.getStaff()));
    }

    public static void main(String [] args) throws IOException, ClassNotFoundException {
        TheGreatestSage sage = new TheGreatestSage();
        sage.change();
    }
}
