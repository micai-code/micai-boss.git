package com.micai.boss.base.design.mode.factory;

/**
 * 描述：Intel的抽象工厂的实现类
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 11:40
 */
public class IntelFactory implements AbstractFactory {

    @Override
    public Cpu createCpu() {
        return new IntelCpu(755);
    }

    @Override
    public Mainboard createMainboard() {
        return new IntelMainboard(755);
    }
}
