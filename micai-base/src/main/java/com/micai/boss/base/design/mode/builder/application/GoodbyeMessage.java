package com.micai.boss.base.design.mode.builder.application;

/**
 * 描述：具体产品类GoodbyeMessage
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 15:53
 */
public class GoodbyeMessage extends AutoMessage {

    public GoodbyeMessage() {
        System.out.println("发送欢送信息");
    }
}
