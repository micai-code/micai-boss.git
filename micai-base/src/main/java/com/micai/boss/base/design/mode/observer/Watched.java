package com.micai.boss.base.design.mode.observer;

import java.util.Observable;

/**
 * @author zhaoxinguo on 2018/4/29.
 * @email sxdtzhaoxinguo@163.com
 * @qq 827358369
 * @phone 18611966723
 * @description 被观察者Watched类源代码
 */
public class Watched extends Observable {

    private String data = "start";

    public String getData() {
        return data;
    }

    public void setData(String data) {
        if (!this.data.equals(data)) {
            setChanged();
        }
        notifyObservers();
    }
}
