package com.micai.boss.base.mapper;

import java.io.Serializable;
import java.util.List;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/24 15:56
 */
public interface BaseMapper<T extends Serializable, ID extends Serializable> {

    long insert(T entity);

    int update(T entity);

    T findOne(ID id);

    List<T> findAll();

    long count();

    List<T> getByIds(Iterable<ID> ids);
}
