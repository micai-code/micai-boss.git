package com.micai.boss.base.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/5/4 15:21
 */
public class BaseInterceptor extends HandlerInterceptorAdapter {

    protected static final Logger logger = LoggerFactory.getLogger(BaseInterceptor.class);

    /**
     * 在控制器执行前调用
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        logger.info("01<--执行preHandle方法-->01");
        String requestURI = request.getRequestURI();
        logger.info("requestURI：" + requestURI);
        StringBuffer requestURL = request.getRequestURL();
        logger.info("requestURL：" + requestURL);
        return false;
    }

    /**
     * 在控制器执行后调用
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        logger.info("02<--执行postHandle方法-->02");
        super.postHandle(request, response, handler, modelAndView);
    }

    /**
     * 在整个请求执行完成后调用
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        logger.info("03<--执行afterCompletion方法-->03");
        super.afterCompletion(request, response, handler, ex);
    }

}
