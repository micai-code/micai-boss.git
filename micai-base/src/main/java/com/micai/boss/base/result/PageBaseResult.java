package com.micai.boss.base.result;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/23 17:59
 */
public class PageBaseResult<T> extends BaseResult<T> {

    private static final long serialVersionUID = 1L;

    private final static int defaultPageSize = 15;//每页默认条数

    private long totalRows = 0;// 总行数
    private long totalPage = 1;//总页数
    private int pageSize;
    private int pageNum = 1;//当前页码
    private String url;

    public PageBaseResult(int pageSize){
        this.pageSize = pageSize;
    }

    public long getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(long totalRows) {
        this.totalRows = totalRows;
    }

    public long getTotalPage() {
        return  (int) Math.ceil(this.totalRows / this.pageSize) + (this.totalRows % this.pageSize > 0 ? 1:0);
    }

    public void setTotalPage(long totalPage) {
        this.totalPage = totalPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
        if(pageSize<=0){
            this.pageSize = PageBaseResult.defaultPageSize;
        }
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
