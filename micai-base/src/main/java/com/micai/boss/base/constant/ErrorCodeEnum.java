package com.micai.boss.base.constant;

/**
 * 描述：系统错误码
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/19 17:08
 */
public enum ErrorCodeEnum {

    系统异常(999, "系统繁忙,请稍后重试!"),
    系统提醒(888,"系统提醒"),

    参数异常(700,"参数异常."),
    参数丢失(710,"参数丢失."),
    参数为空(721,"参数为空."),
    参数不为空(722,"参数不为空."),
    非法参数(720,"参数非法"),

    //验证码
    获取验证码异常(600,"获取验证码异常,请稍后重试."),
    校验验证码异常(601, "校验验证码异常,请稍后重试."),

    获取验证码冻结(610,"获取验证码冻结"),
    获取验证码冻结3H(611,"您当前频繁获取验证码,请3小时后重新获取"),
    获取验证码冻结60s(612,"您当前频繁获取验证码,请60秒后重新获取"),

    验证码错误(620,"验证码错误,请重新输入"),
    验证码过期(621,"验证码过期,请重新获取"),
    验证码错误次数过多(622,"验证码输入次数过多，请重新获取");

    private int code;
    private String message;

    private ErrorCodeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static boolean contain(Integer value) {
        if (value == null) {
            return false;
        }
        ErrorCodeEnum[] values = ErrorCodeEnum.values();
        for (ErrorCodeEnum sexEnum : values) {
            if (sexEnum.code == value) {
                return true;
            }
        }
        return false;
    }

}
