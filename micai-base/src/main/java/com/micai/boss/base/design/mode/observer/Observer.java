package com.micai.boss.base.design.mode.observer;

/**
 * @author zhaoxinguo on 2018/4/29.
 * @email sxdtzhaoxinguo@163.com
 * @qq 827358369
 * @phone 18611966723
 * @description 抽象观察者角色类
 */
public interface Observer {

    /**
     * 更新接口
     * @param state 更新的状态
     */
//    public void update(String state);

    /**
     * 更新接口
     * @param subject 传入主题对象，方便获取相应的主题对象的状态
     */
    public void update(Subject subject);

}
