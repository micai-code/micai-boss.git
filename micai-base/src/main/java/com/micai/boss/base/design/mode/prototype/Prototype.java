package com.micai.boss.base.design.mode.prototype;

/**
 * 描述：抽象原型角色
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 17:24
 */
public interface Prototype {

    /**
     * 克隆自身的方法
     * @return 一个从自身克隆出来的对象
     */
//    public Object clone();

    public Prototype clone();
    public String getName();
    public void setName(String name);
}
