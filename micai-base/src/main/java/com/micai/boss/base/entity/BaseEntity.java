package com.micai.boss.base.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.micai.boss.base.result.PageResult;
import java.io.Serializable;
import java.util.Date;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/24 15:55
 */
public abstract class BaseEntity implements Serializable {

    @TableId(value="id", type= IdType.ID_WORKER)
    private Long id;
    @TableField("version")
    private Long version = 200L;
    @TableField("create_by")
    private String createBy;
    @TableField("update_by")
    private String updateBy;
    @TableField("create_date")
    private Date createDate;
    @TableField("update_date")
    private Date updateDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        if (createDate == null) {
            this.createDate = new Date();
        } else {
            this.createDate = createDate;
        }
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        if (updateDate == null) {
            this.updateDate = new Date();
        } else {
            this.updateDate = updateDate;
        }
    }
}
