package com.micai.boss.base.design.mode.factory;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 10:44
 */
public class AmdCpu implements Cpu {

    /**
     * CPU的针脚数
     */
    private int pins = 0;

    public AmdCpu(int pins) {
        this.pins = pins;
    }

    @Override
    public void calculate() {
        System.out.println("AMD CPU的针脚数：" + pins);
    }
}
