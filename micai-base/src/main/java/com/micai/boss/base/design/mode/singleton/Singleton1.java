package com.micai.boss.base.design.mode.singleton;

/**
 * 描述：线程不安全（懒汉模式）
 * <p>
 * lazy loading，但是多线程不安全。不推荐
 *
 * @author: 赵新国
 * @date: 2018/4/26 9:58
 */
public class Singleton1 {

    private static Singleton1 instance;

    private Singleton1() {}

    public static Singleton1 getInstance() {
        if (instance == null) {
            instance = new Singleton1();
        }
        return instance;
    }
}
