package com.micai.boss.base.utils.image;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/23 15:31
 */
public class ImagePropertyVO {

    private int width;
    private int height;

    /**
     * @return Returns the width.
     */
    public int getWidth()
    {
        return width;
    }
    /**
     * @param width - The width to set.
     */
    public void setWidth(int width)
    {
        this.width = width;
    }
    /**
     * @return Returns the height.
     */
    public int getHeight()
    {
        return height;
    }
    /**
     * @param height - The height to set.
     */
    public void setHeight(int height)
    {
        this.height = height;
    }
}
