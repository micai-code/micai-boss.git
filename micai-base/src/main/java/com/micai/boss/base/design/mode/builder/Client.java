package com.micai.boss.base.design.mode.builder;

/**
 * 描述：客户端类Client
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 15:36
 */
public class Client {

    public static void main(String [] args) {
        Builder builder = new ConcreteBuilder();
        Director director = new Director(builder);
        director.construct();
        Product product = builder.retrieveResult();
        System.out.println(product.toString());
    }
}
