package com.micai.boss.base.constant;

/**
 * 描述：客户端枚举
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/23 17:47
 */
public enum  ClientSourceEnum {

    Android(1, "Android"),
    IOS(2, "IOS"),
    H5(3,"H5"),
    PC(4,"PC"),
    未知(9, "未知");

    private int code;
    private String message;

    private ClientSourceEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static boolean contain(Integer value) {
        if (value == null) {
            return false;
        }
        ClientSourceEnum[] values = ClientSourceEnum.values();
        for (ClientSourceEnum sexEnum : values) {
            if (sexEnum.code == value) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取客户端类型
     * @param clientName
     * @return
     */
    public static ClientSourceEnum getClientSourceEnum(String clientName){
        if (ClientSourceEnum.Android.getMessage().equalsIgnoreCase(clientName)){
            return ClientSourceEnum.Android;
        }else if (ClientSourceEnum.IOS.getMessage().equalsIgnoreCase(clientName)){
            return ClientSourceEnum.IOS;
        }else if (ClientSourceEnum.H5.getMessage().equalsIgnoreCase(clientName)){
            return ClientSourceEnum.H5;
        }else{
            return ClientSourceEnum.未知;
        }
    }

}
