package com.micai.boss.base.design.mode.singleton;

/**
 * 描述：线程安全（懒汉模式）
 * <p>
 * lazy loading，线程安全，但是效率极其低下，同步锁锁的是对象，每次取对象都有加锁，因此不推荐，性能很低。
 *
 * @author: 赵新国
 * @date: 2018/4/26 10:01
 */
public class Singleton2 {

    private static Singleton2 instance;

    private Singleton2() {}

    public static synchronized Singleton2 getInstance() {
        if (instance == null) {
            instance = new Singleton2();
        }
        return instance;
    }

}
