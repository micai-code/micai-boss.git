package com.micai.boss.base.design.mode.observer;

/**
 * @author zhaoxinguo on 2018/4/29.
 * @email sxdtzhaoxinguo@163.com
 * @qq 827358369
 * @phone 18611966723
 * @description 具体主题角色类
 */
public class ConcreteSubject extends Subject {

    private String state;

    public String getState() {
        return state;
    }

    /*public void change(String newState) {
        state = newState;
        System.out.println("主题状态为：" + state);
        // 状态发生改变，通知各个观察者对象
        this.nodifyObservers(state);
    }*/

    public void change(String newState) {
        state = newState;
        System.out.println("主题状态为：" + state);
        // 状态发生改变，通知各个观察者对象
        this.nodifyObservers();
    }


}
