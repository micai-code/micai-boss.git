package com.micai.boss.base.design.mode.factory;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 10:42
 */
public class IntelCpu implements Cpu {

    /**
     * CPU的针脚数
     */
    private int pins = 0;

    public IntelCpu(int pins) {
        this.pins = pins;
    }

    @Override
    public void calculate() {
        System.out.println("Intel CPU的针脚数：" + pins);
    }
}
