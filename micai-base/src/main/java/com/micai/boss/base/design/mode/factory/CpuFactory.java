package com.micai.boss.base.design.mode.factory;

/**
 * 描述：CPU工厂类
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 11:03
 */
public class CpuFactory {

    public static Cpu createCpu(int type) {
        Cpu cpu = null;
        if (type == 1) {
            cpu = new IntelCpu(755);
        } else if (type == 2) {
            cpu = new AmdCpu(938);
        }
        return cpu;
    }

}
