package com.micai.boss.base.design.mode.singleton;

/**
 * 描述：线程安全（饿汉模式）
 * <p>
 * 这种方式基于classloder机制避免了多线程的同步问题，不过，instance在类装载时就实例化，这时候初始化instance显然没有达到lazy loading的效果。不推荐。
 *
 * @author: 赵新国
 * @date: 2018/4/26 10:02
 */
public class Singleton3 {

    private static Singleton3 instance = new Singleton3();

    private Singleton3() {}

    public static synchronized Singleton3 getInstance() {
        return instance;
    }
}
