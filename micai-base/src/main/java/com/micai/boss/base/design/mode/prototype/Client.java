package com.micai.boss.base.design.mode.prototype;

/**
 * 描述：客户端角色
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 17:27
 */
public class Client {

//    /**
//     * 持有需要使用的原型接口对象
//     */
//   private Prototype prototype;
//
//    /**
//     * 构造方法，传入需要使用的原型接口对象
//     * @param prototype
//     */
//   public Client(Prototype prototype) {
//       this.prototype = prototype;
//   }
//
//   public void operation(Prototype prototype) {
//       // 需要创建原型接口的对象
//       Prototype copyPrototype = (Prototype) prototype.clone();
//   }

    public static void main(String [] args) {

        try {
            Prototype prototype1 = new ConcretePrototype1();
            PrototypeManager.setPrototype("p1", prototype1);
            //获取原型来创建对象
            Prototype p2 = PrototypeManager.getPrototype("p1").clone();
            p2.setName("张三");
            System.out.println("第一个实例：" + p2);

            //有人动态的切换了实现
            Prototype prototype2 = new ConcretePrototype2();
            PrototypeManager.setPrototype("p1", prototype2);
            //重新获取原型来创建对象
            Prototype p3 = PrototypeManager.getPrototype("p1").clone();
            p3.setName("李四");
            System.out.println("第二个实例：" + p3);

            //有人注销了这个原型
            PrototypeManager.removePrototype("p1");
            //再次获取原型来创建对象
            Prototype p4 = PrototypeManager.getPrototype("p1").clone();
            p4.setName("王五");
            System.out.println("第三个实例：" + p4);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}

