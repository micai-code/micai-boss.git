package com.micai.boss.base.encrypt;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/23 15:43
 */
public class ConfigureEncryptAndDecrypt {

    public static final String CHAR_ENCODING = "UTF-8";
    public static final String AES_ALGORITHM = "AES/ECB/PKCS5Padding";
    public static final String RSA_ALGORITHM = "RSA/ECB/PKCS1Padding";
}
