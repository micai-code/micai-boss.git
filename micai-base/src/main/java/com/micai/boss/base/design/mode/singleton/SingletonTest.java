package com.micai.boss.base.design.mode.singleton;

import java.util.concurrent.CountDownLatch;

/**
 * 描述：单例模式可以使用多线程并发进行测试
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 9:42
 */
public class SingletonTest {

    /**
     * 其中CountDownLatch latch为闭锁，所有线程中都用latch.await();等待锁释放，待所有线程初始化完成使用latch.countDown();释放锁，
     * 从而达到线程并发执行Singleton.getInstance()的效果
     * @param args
     */
    public static void main(String [] args) {

        final CountDownLatch latch = new CountDownLatch(1);
        int threadCount = 1000;

        for (int i=0; i<threadCount; i++) {
            new Thread() {
                @Override
                public void run() {
                    try {
                        // all thread to wait
                        latch.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    // api get instance
                    System.out.println(Singleton.getInstance().hashCode());
                }
            }.start();
        }
        //  release lock, let all thread excute Singleton.getInstance() at the same time
        latch.countDown();
    }
}
