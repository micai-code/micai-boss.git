package com.micai.boss.base.design.mode.adapter;

/**
 * 描述：目标角色的源代码
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/28 11:40
 */
public interface Target {

    /**
     * 这是源类Adaptee也有的方法
     */
    public void sampleOperation1();

    /**
     * 这是源类Adapteee没有的方法
     */
    public void sampleOperation2();

}
