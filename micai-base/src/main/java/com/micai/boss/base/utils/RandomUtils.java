package com.micai.boss.base.utils;

import java.util.Random;

/**
 * 描述：生成随机数工具类
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/23 17:40
 */
public class RandomUtils {

    final static Random random = new Random();

    public static Integer getRandom4(){
        return random.nextInt(9000) + 1000;
    }

    public static Integer getRandom5(){
        return random.nextInt(90000) + 10000;
    }

    public static Integer getRandom6(){
        return random.nextInt(900000) + 100000;
    }

    public static Integer getRandom8(){
        return random.nextInt(90000000) + 10000000;
    }

}
