package com.micai.boss.base.service.validate;

import com.micai.boss.base.exception.ServiceException;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/23 17:53
 */
public interface ValidationService {

    ValidationResult validate(Object param, boolean fastMode);

    void validate(Object param) throws ServiceException;
}
