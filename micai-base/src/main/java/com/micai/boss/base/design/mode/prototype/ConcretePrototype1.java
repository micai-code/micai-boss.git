package com.micai.boss.base.design.mode.prototype;

/**
 * 描述：具体原型角色
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 17:24
 */
public class ConcretePrototype1 implements Prototype {

    private String name;

    @Override
    public Prototype clone() {
        // 最简单的克隆，新建一个自身对象，由于没有属性就不再复制值了
        Prototype prototype = new ConcretePrototype1();
        prototype.setName(this.name);
        return prototype;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ConcretePrototype1{" +
                "name='" + name + '\'' +
                '}';
    }
}
