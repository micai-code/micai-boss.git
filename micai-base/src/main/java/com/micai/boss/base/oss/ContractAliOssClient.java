package com.micai.boss.base.oss;

import java.io.InputStream;

import org.apache.commons.lang.StringUtils;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/23 14:39
 */
public class ContractAliOssClient extends AbstractAliOssClient {

    public ContractAliOssClient(String bucketName, String endpoint, String accessKeyId, String accessKeySecret) {
        super(bucketName, endpoint, accessKeyId, accessKeySecret);
    }

    /**
     * 上传借款合同到阿里云
     *
     * @param content
     * @param key
     * @return
     */
    public boolean putContractObject(InputStream content, String key) {
        return putObject(content, key);
    }

    /**
     * 获得合同输入流 根据在oss中的key和文件类型获得输入流<br>
     * 如果获取的key不存在或者发生其它异常会返回null
     *
     * @param key
     * @return
     */
    public InputStream getContractInputStream(String key) {
        if (StringUtils.isBlank(key)) {
            return null;
        }
        return getInputStream(key);
    }

}
