package com.micai.boss.base.cache;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.SilentCssErrorHandler;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import java.io.IOException;

public class HtmlUnitTest {
    public static void main(String... args) throws IOException {
        final WebClient webClient = new WebClient(BrowserVersion.CHROME);
        webClient.setCssErrorHandler(new SilentCssErrorHandler());
        webClient.setAjaxController(new NicelyResynchronizingAjaxController());
        webClient.getOptions().setRedirectEnabled(true);
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);//当HTTP的状态非200时是否抛出异常
        //设置浏览器的请求超时时间
        webClient.getOptions().setTimeout(30000);
        //是否启用CSS
        webClient.getOptions().setCssEnabled(false);
        //是否启用JavaScript
        webClient.getOptions().setJavaScriptEnabled(true);
        //设置JS后台等待执行时间
        webClient.waitForBackgroundJavaScript(100000);
        //设置JS执行的超时时间
        webClient.setJavaScriptTimeout(100000);
        //JavaScript执行出现异常了，当前操作是否抛出异常
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.setAjaxController(new NicelyResynchronizingAjaxController());


        for(int i=0; i < 20; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for(int i=0; i < 3; i++) {
                        try {
                            HtmlPage page = webClient.getPage("http://fund.eastmoney.com/f10/jjjz_000200.html");
                            System.out.println(page.asXml());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
        }
        webClient.closeAllWindows();
    }
}
