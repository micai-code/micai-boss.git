package com.micai.boss.base.design.mode.factory;

/**
 * 描述：抽象工厂接口
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 11:38
 */
public interface AbstractFactory {

    /**
     * 创建CPU对象
     *
     * @return CPU对象
     */
    public Cpu createCpu();

    /**
     * 创建主板对象
     *
     * @return 主板对象
     */
    public Mainboard createMainboard();
}
