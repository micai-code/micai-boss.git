package com.micai.boss.base.design.mode.builder.application;

/**
 * 描述：具体建造者WelcomeBuilder
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 16:02
 */
public class WelcomeBuilder extends Builder {

    public WelcomeBuilder() {
        autoMessage = new WelcomeMessage();
    }

    @Override
    public void buildSubject() {
        autoMessage.setSubject("欢迎标题");
    }

    @Override
    public void buildBody() {
        autoMessage.setBody("欢迎内容");
    }

}
