package com.micai.boss.base.exception;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/19 17:06
 */
public class ServiceException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private Integer code = 999;

    public ServiceException() {
        super();
    }

    public ServiceException(String message){
        super(message);
    }

    public ServiceException(Integer code, String message){
        super(message);
        this.code = code;
    }

    public ServiceException(String message, Throwable cause){
        super(message,cause);
    }

    public Integer getCode() {
        return code;
    }
}
