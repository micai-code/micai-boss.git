package com.micai.boss.base.design.mode.template.method;

/**
 * 描述：具体模板角色类 定期存款(Certificate of Deposite)账号
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/28 18:03
 */
public class CDAccount extends Account {

    @Override
    protected String doCalculateAccountType() {
        return "Certificate of Deposite";
    }

    @Override
    protected double doCalculateInterestRate() {
        return 0.06;
    }
}
