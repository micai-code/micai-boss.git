package com.micai.boss.base.design.mode.singleton;

/**
 * 描述：线程安全（饿汉模式）
 * <p>
 * 等同于第三种 也是基于classloder机制避免了多线程的同步问题，不过，instance在类装载时就实例化，这时候初始化instance显然没有达到lazy loading的效果。不推荐。
 * @author: 赵新国
 * @date: 2018/4/26 10:06
 */
public class Singleton4 {

    private static Singleton4 instance;

    static {
        instance = new Singleton4();
    }

    private Singleton4() {}

    public static synchronized Singleton4 getInstance() {
        return instance;
    }


}
