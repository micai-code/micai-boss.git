package com.micai.boss.base.design.mode.factory;

/**
 * 描述：Amd的抽象工厂的实现类
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 11:42
 */
public class AmdFactory implements AbstractFactory {

    @Override
    public Cpu createCpu() {
        return new AmdCpu(938);
    }

    @Override
    public Mainboard createMainboard() {
        return new AmdMainboard(938);
    }
}
