package com.micai.boss.base.design.mode.adapter;

/**
 * 描述：类适配器模式
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/28 11:44
 */
public class Adapter extends Adaptee implements Target {

    /**
     * 由于源类Adaptee没有方法sampleOperation2()
     * 因此适配器补充上这个方法
     */
    @Override
    public void sampleOperation2() {
        // TODO 写相关的代码

    }
}
