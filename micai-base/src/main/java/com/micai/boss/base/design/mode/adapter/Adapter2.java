package com.micai.boss.base.design.mode.adapter;

/**
 * 描述：对象适配器模式
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/28 11:44
 */
public class Adapter2 {

    private Adaptee adaptee;

    public Adapter2(Adaptee adaptee) {
        this.adaptee = adaptee;
    }

    /**
     * 源类Adaptee有方法sampleOperation1
     * 因此适配器类直接委派即可
     */
    public void sampleOperation1() {
        this.adaptee.sampleOperation1();
    }

    /**
     * 源类Adaptee没有方法sampleOperation2
     * 因此由适配器类需要补充此方法
     */
    public void sampleOperation2() {
        // TODO 写相关的代码

    }
}
