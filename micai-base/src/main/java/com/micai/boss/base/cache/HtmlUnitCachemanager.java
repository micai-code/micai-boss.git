//package com.micai.boss.base.cache;
//
//import com.gargoylesoftware.htmlunit.BrowserVersion;
//import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
//import com.gargoylesoftware.htmlunit.SilentCssErrorHandler;
//import com.gargoylesoftware.htmlunit.WebClient;
//import com.micai.boss.base.cache.CacheManager;
//import com.micai.boss.base.cache.ClearCallback;
//
///**
// * @Author Lanxiaowei
// * @Date 2018-04-25 15:07
// * @Description HtmlUnit的WebClient类的缓存管理器
// */
//public class HtmlUnitCachemanager {
//    public static final String CACHE_KEY = "htmlunit";
//    private static final int EXPIRE_TIME = 1800;
//
//    private HtmlUnitCachemanager() {
//
//    }
//
//    private static class SingletonHolder {
//        private static final HtmlUnitCachemanager INSTANCE = new HtmlUnitCachemanager();
//    }
//
//    public static final HtmlUnitCachemanager getInstance() {
//        return HtmlUnitCachemanager.SingletonHolder.INSTANCE;
//    }
//
//    public WebClient getWebClient() {
//        return CacheManager.getData(CACHE_KEY, new CacheManager.Load<WebClient>() {
//            @Override
//            public WebClient load() {
//                WebClient webClient = new WebClient(BrowserVersion.CHROME);
//                webClient.setCssErrorHandler(new SilentCssErrorHandler());
//                webClient.setAjaxController(new NicelyResynchronizingAjaxController());
//                webClient.getOptions().setRedirectEnabled(true);
//                webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);//当HTTP的状态非200时是否抛出异常
//                //设置浏览器的请求超时时间
//                webClient.getOptions().setTimeout(30000);
//                //是否启用CSS
//                webClient.getOptions().setCssEnabled(false);
//                //是否启用JavaScript
//                webClient.getOptions().setJavaScriptEnabled(true);
//                //设置JS后台等待执行时间
//                webClient.waitForBackgroundJavaScript(100000);
//                //设置JS执行的超时时间
//                webClient.setJavaScriptTimeout(100000);
//                //JavaScript执行出现异常了，当前操作是否抛出异常
//                webClient.getOptions().setThrowExceptionOnScriptError(false);
//                webClient.setAjaxController(new NicelyResynchronizingAjaxController());
//                return webClient;
//            }
//        },new ClearCallback<WebClient>() {
//            @Override
//            public void close(WebClient webClient) {
//                if(null != webClient) {
//                    webClient.closeAllWindows();
//                }
//            }
//        },EXPIRE_TIME);
//    }
//
//}
