package com.micai.boss.base.design.mode.builder.application;

/**
 * 描述：客户端Client
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 16:11
 */
public class Client {

    public static void main(String [] args) {
        Builder builder = new WelcomeBuilder();
        Director director = new Director(builder);
        director.construct("toAddress@126.com", "fromAddress@126.com");

        Builder builder1 = new GoodbyeBuilder();
        Director director1 = new Director(builder1);
        director1.construct("toAddress@126.com", "fromAddress@126.com");
    }

}
