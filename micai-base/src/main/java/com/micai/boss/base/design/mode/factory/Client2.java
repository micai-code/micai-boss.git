package com.micai.boss.base.design.mode.factory;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 11:46
 */
public class Client2 {

    public static void main(String [] args) {
        //创建装机工程师对象
        ComputerEngineer2 computerEngineer2 = new ComputerEngineer2();
        //客户选择并创建需要使用的产品对象
        AbstractFactory abstractFactory = new AmdFactory();
        //告诉装机工程师自己选择的产品，让装机工程师组装电脑
        computerEngineer2.makeComputer(abstractFactory);
    }
}
