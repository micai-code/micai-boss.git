package com.micai.boss.base.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 描述：字符串中英文处理工具类
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/28 9:15
 */
public class StringCnEsUtils {

    /**
     * 是否是中文
     * @param c
     * @return
     */
    public static boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
            return true;
        }
        return false;
    }

    /**
     * 是否是英文
     * @param charaString
     * @return
     */
    public static boolean isEnglish(String charaString) {
        return charaString.matches("^[a-zA-Z]*");
    }

    /**
     * 是否是英文
     * @param str
     * @return
     */
    public static boolean isChinese(String str){
        String regEx = "[\\u4e00-\\u9fa5]+";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        if(m.find()) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        System.out.println(isChinese('员'));
        System.out.println(isChinese('s'));
        System.out.println(isEnglish("程序员之家"));
        System.out.println(isEnglish("robert"));
        System.out.println(isChinese(" 程序员论坛"));
    }

}
