package com.micai.boss.base.design.mode.builder.application;

/**
 * 描述：具体产品类WelcomeMessage
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/26 15:52
 */
public class WelcomeMessage extends AutoMessage {

    public WelcomeMessage() {
        System.out.println("发送欢迎信息");
    }
}
