package com.micai.boss.base.cache;

/**
 * @Author Lanxiaowei
 * @Date 2018-04-25 16:11
 * @Description 缓存对象清理之后执行的回调接口,用于回收被清理对象占用的系统资源
 */
public interface ClearCallback<T> {
    //资源回收
    void close(T t);
}
