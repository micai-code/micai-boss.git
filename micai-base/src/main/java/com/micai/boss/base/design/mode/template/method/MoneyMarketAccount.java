package com.micai.boss.base.design.mode.template.method;

/**
 * 描述：具体模板角色类 货币市场(Money Market)账号
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/28 18:02
 */
public class MoneyMarketAccount extends Account {

    @Override
    protected String doCalculateAccountType() {
        return "Money Market";
    }

    @Override
    protected double doCalculateInterestRate() {
        return 0.045;
    }

}
