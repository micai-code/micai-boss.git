package com.micai.boss.base.design.mode.observer;

/**
 * @author zhaoxinguo on 2018/4/29.
 * @email sxdtzhaoxinguo@163.com
 * @qq 827358369
 * @phone 18611966723
 * @description 具体观察者角色类
 */
public class ConcreteObserver implements Observer {

    // 观察者的状态
    private String observerState;

    /*@Override
    public void update(String state) {
        // 更新观察者的状态，使其与目标的状态保持一致
        observerState = state;
        System.out.println("状态为：" + observerState);
    }*/

    @Override
    public void update(Subject subject) {
        // 更新观察者的状态，使其与目标的状态保持一致
        observerState = ((ConcreteSubject)subject).getState();
        System.out.println("观察者状态为：" + observerState);
    }
}
