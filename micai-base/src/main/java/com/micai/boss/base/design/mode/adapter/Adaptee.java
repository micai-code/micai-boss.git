package com.micai.boss.base.design.mode.adapter;

/**
 * 描述：源角色Adaptee
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/28 11:43
 */
public class Adaptee {

    public void sampleOperation1(){

    }
}
