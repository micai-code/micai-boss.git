DROP TABLE IF EXISTS `sms_template`;
CREATE TABLE `sms_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(50) DEFAULT NULL COMMENT '渠道编码',
  `template` varchar(200) DEFAULT NULL COMMENT '模板内容',
  `provider_name` varchar(50) DEFAULT NULL COMMENT '通道名称',
  `version` bigint(20) NOT NULL DEFAULT '200' COMMENT '版本号',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='短信模板表';

-- ----------------------------
-- Records of sms_template
-- ----------------------------
INSERT INTO `sms_template` VALUES ('1', 'yimei', '您的借款在${date}前应还款人民币${amount}元，当前已逾期并产生滞纳金和违约金，请您及时还款避免产生更多滞纳金', 'yimei', '200', '2016-05-09 15:43:52', '2017-01-10 17:20:30');