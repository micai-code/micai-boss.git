package com.micai.boss.user;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.concurrent.CountDownLatch;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/2/24 9:40
 */
@SpringBootApplication
@MapperScan(basePackages = {"com.micai.boss.user"})
public class UserProviderApplication implements CommandLineRunner, DisposableBean {

    private  final Logger log = LoggerFactory.getLogger(UserProviderApplication.class);

    private final static CountDownLatch latch = new CountDownLatch(1);

    private static ConfigurableApplicationContext context;

    public static void main(String [] args) throws InterruptedException {
        context = SpringApplication.run(UserProviderApplication.class, args);
        latch.await();
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("用户------>>服务提供者启动完毕------>>启动完毕");
    }

    @Override
    public void destroy() throws Exception {
        latch.countDown();
        context.close();
        log.info("用户------>>服务提供者关闭------>>服务关闭");
    }

}
