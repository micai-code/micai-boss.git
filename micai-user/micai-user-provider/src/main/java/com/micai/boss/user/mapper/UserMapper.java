package com.micai.boss.user.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.micai.boss.user.entity.UserEntity;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/2/24 11:26
 */
public interface UserMapper extends BaseMapper<UserEntity> {

}
