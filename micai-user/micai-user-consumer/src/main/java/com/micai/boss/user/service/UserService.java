package com.micai.boss.user.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.micai.boss.base.result.BaseResult;
import com.micai.boss.redis.service.AbstractRedisService;
import com.micai.boss.user.dto.UserDto;
import com.micai.boss.user.provider.UserProvider;
import com.micai.boss.user.vo.UserVo;
import com.micai.boss.weixin.service.WeixinService;
import com.micai.boss.weixin.service.dto.AccessTokenDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/2/24 9:58
 */
@Component
public class UserService extends AbstractRedisService<UserDto> {

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Reference(version = "1.0.0")
    private UserProvider userProvider;

    @Reference(version = "1.0.0")
    private WeixinService weixinService;

    @Override
    protected String getRedisKey() {
        return "USER_KEY_";
    }

    public UserVo getById(Long id) {
        UserVo userVo = new UserVo();
        Object obj = super.get(getRedisKey() + id);
        if (obj != null) {
            UserDto userDto = (UserDto) obj;
            BeanUtils.copyProperties(userDto, userVo);
        } else {
            UserDto userDto = userProvider.getById(id);
            if (userDto == null) {
                return null;
            }
            super.put(getRedisKey() + id, userDto, 1200);
            BeanUtils.copyProperties(userDto, userVo);
        }
        return userVo;
    }

    public List<UserVo> getList() {
        List<UserDto> userDtos = userProvider.getList();
        if (userDtos == null) {
            return null;
        }
        List<UserVo> userVos = new ArrayList<>();
        for (UserDto userDto : userDtos) {
            UserVo userVo = new UserVo();
            BeanUtils.copyProperties(userDto, userVo);
            userVos.add(userVo);
        }
        return userVos;
    }

    public void save(UserVo userVo) {
        UserDto userDto = new UserDto();
        BeanUtils.copyProperties(userVo, userDto);
        userProvider.save(userDto);
    }

    public void update(UserVo userVo) {
        UserDto userDto = new UserDto();
        BeanUtils.copyProperties(userVo, userDto);
        userProvider.update(userDto);
        // 如果缓存存储，删除
        if (super.get(getRedisKey() + userVo.getId()) != null) {
            super.remove(getRedisKey() + userVo.getId());
        }
    }

    public void remove(Long id) {
        userProvider.remove(id);
        // 如果缓存存储，删除
        if (super.get(getRedisKey() + id) != null) {
            super.remove(getRedisKey() + id);
        }
    }

    public BaseResult<AccessTokenDto> getAccessToken() {
        BaseResult<AccessTokenDto> accessToken = weixinService.getAccessToken();
        logger.info("accessToken: {}", JSONObject.toJSONString(accessToken));
        return accessToken;
    }
}
