package com.micai.boss.user.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.micai.boss.mongodb.service.AbstractMongodbService;
import com.micai.boss.user.dto.UserDto;
import com.micai.boss.user.provider.UserProvider;
import com.micai.boss.user.vo.UserVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/2/28 16:11
 */
@Component
public class UserMongodbService extends AbstractMongodbService<UserDto> {

    @Reference(version = "1.0.0")
    private UserProvider userProvider;

    public void save(UserVo userVo) {
        UserDto userDto = new UserDto();
        BeanUtils.copyProperties(userVo, userDto);
        super.save(userDto);
    }

    public List<UserVo> getList() {
        List<UserDto> userDtos = super.findAll();
        if (userDtos == null) {
            return null;
        }
        List<UserVo> userVos = new ArrayList<>();
        for (UserDto userDto : userDtos) {
            UserVo userVo = new UserVo();
            BeanUtils.copyProperties(userDto, userVo);
            userVos.add(userVo);
        }
        return userVos;
    }
}
