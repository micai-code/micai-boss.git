package com.micai.boss.user.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/25 15:00
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig extends WebMvcConfigurationSupport {

    @Bean
    public Docket userApi() {
        return new Docket(DocumentationType.SWAGGER_2)
            .groupName("用户API接口文档")
            .apiInfo(metaData())
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.micai.boss.user.modules.user"))
            .paths(PathSelectors.any()).build();
    }

    @Bean
    public Docket weixinApi() {
        return new Docket(DocumentationType.SWAGGER_2)
            .groupName("微信API接口文档")
            .apiInfo(metaData())
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.micai.boss.user.modules.weixin"))
            .paths(PathSelectors.any()).build();
    }

    private ApiInfo metaData() {
        return new ApiInfoBuilder()
            .title("Spring Boot REST API")
            .description("\"Spring Boot REST API for Online Store\"")
            .version("1.0.0")
            .license("Apache License Version 2.0")
            .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0\"")
            .contact(new Contact("micai", "https://blog.csdn.net/sxdtzhaoxinguo", "sxdtzhaoxinguo@163.com"))
            .build();
    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}
