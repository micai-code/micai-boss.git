package com.micai.boss.user.modules.user.controller;

import com.micai.boss.user.service.UserService;
import com.micai.boss.user.vo.UserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/2/24 10:06
 */
@Api(value = "用户管理", description = "用户管理")
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation(value="查询用户", notes="根据ID查询用户")
    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public UserVo get(@PathVariable Long id) {
        UserVo userVo = userService.getById(id);
        return userVo;
    }

    @ApiOperation(value="获取用户列表", notes="查询全部用户列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<UserVo> list(Model model) {
        List<UserVo> userVos = userService.getList();
        return userVos;
    }

    @ApiOperation(value="创建用户", notes="根据User对象创建用户")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public void save(@RequestBody UserVo userVo) {
        userService.save(userVo);
    }

    @ApiOperation(value="删除用户", notes="根据ID删除用户")
    @RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
    public void remove(@PathVariable Long id) {
        if (userService.getById(id) != null) {
            userService.remove(id);
        }
    }

    @ApiOperation(value="更新用户", notes="根据ID更新用户")
    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public void update(@PathVariable Long id, @RequestBody UserVo userVo) {
        if (userService.getById(id) != null) {
            userVo.setId(id);
            userVo.setUpdateDate(new Date());
            userService.update(userVo);
        }
    }

}
