package com.micai.boss.user.modules.user.controller;

import com.micai.boss.user.service.UserMongodbService;
import com.micai.boss.user.vo.UserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/2/28 16:18
 */
@Api(value = "用户管理M", description = "用户管理M")
@RestController
@RequestMapping("/user/m")
public class UserMongodbController {

    @Autowired
    private UserMongodbService userMongodbService;

    @ApiOperation(value="获取用户列表", notes="获取用户列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<UserVo> list() {
        List<UserVo> userVos = userMongodbService.getList();
        return userVos;
    }

    @ApiOperation(value="创建用户", notes="根据User对象创建用户")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public void save(@RequestBody UserVo userVo) {
        userMongodbService.save(userVo);
    }
}
