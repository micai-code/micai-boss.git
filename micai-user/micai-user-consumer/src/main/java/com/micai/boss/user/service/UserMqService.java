package com.micai.boss.user.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.micai.boss.rabbitmq.service.AbstractRabbitMQService;
import com.micai.boss.redis.service.AbstractRedisService;
import com.micai.boss.user.dto.UserDto;
import com.micai.boss.user.provider.UserProvider;
import com.micai.boss.user.vo.UserVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/2/24 9:58
 */
@Component
public class UserMqService extends AbstractRabbitMQService {

}
