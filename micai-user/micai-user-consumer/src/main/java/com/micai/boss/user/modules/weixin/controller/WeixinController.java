package com.micai.boss.user.modules.weixin.controller;

import com.micai.boss.base.result.BaseResult;
import com.micai.boss.user.service.UserService;
import com.micai.boss.weixin.service.dto.AccessTokenDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/25 10:48
 */
@Api(value = "微信管理", description = "微信管理")
@RestController
@RequestMapping("/weixin")
public class WeixinController {

    @Autowired
    private UserService userService;

    @ApiOperation(value="获取微信公众号token", notes="获取微信公众号token")
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public BaseResult<AccessTokenDto> get() {
        BaseResult<AccessTokenDto> accessToken = userService.getAccessToken();
        return accessToken;
    }
}
