package com.micai.boss.user.modules.user.controller;

import com.alibaba.fastjson.JSONObject;
import com.micai.boss.user.service.UserMqService;
import com.micai.boss.user.service.UserService;
import com.micai.boss.user.vo.UserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/2/28 17:18
 */
@Api(value = "用户管理MQ", description = "用户管理MQ")
@RestController
@RequestMapping("/user/mq")
public class UserMqController {

    private static final Logger logger = LoggerFactory.getLogger(UserMqController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserMqService userMqService;

    @ApiOperation(value="获取用户详细信息", notes="根据url的id来获取用户详细信息")
    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public UserVo get(@PathVariable Long id) {
        UserVo userVo = userService.getById(id);
        if (userVo != null) {
            // 发送消息
            userMqService.convertAndSend(JSONObject.toJSONString(userVo));
            logger.info("消息发送成功: {}", JSONObject.toJSONString(userVo));
        }
        return userVo;
    }
}
