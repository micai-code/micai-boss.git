package com.micai.boss.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.concurrent.CountDownLatch;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/2/24 9:40
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.micai.boss"})
public class UserConsumerApplication implements CommandLineRunner, DisposableBean {

    private  final Logger log = LoggerFactory.getLogger(UserConsumerApplication.class);

    private final static CountDownLatch latch = new CountDownLatch(1);

    private static ConfigurableApplicationContext context;

    public static void main(String [] args) throws InterruptedException {
        SpringApplication.run(UserConsumerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("用户------>>服务消费者启动完毕------>>启动完毕");
    }

    @Override
    public void destroy() throws Exception {
        latch.countDown();
        context.close();
        log.info("用户------>>服务提供者关闭------>>服务关闭");
    }
}

