package com.micai.boss.user.provider;

import com.micai.boss.user.dto.UserDto;

import java.util.List;
import java.util.Map;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/2/24 9:34
 */
public interface UserProvider {

    UserDto getById(Long id);

    List<UserDto> getList();

    void save(UserDto userDto);

    void update(UserDto userDto);

    void remove(Long id);

    Map<String, Object> getTaleData(int pageNum, int pageSize, String userName);
}
