package com.micai.boss.sms.queue.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Map;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/24 11:50
 */
public class ApplicationUtil implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ApplicationUtil.applicationContext = applicationContext;
    }

    public static Object getBean(String name){
        return applicationContext.getBean(name);
    }

    /**
     * 根据类型获取bean
     * @param clas
     * @param <T>
     * @return
     */
    public static <T> Map<String, T> getBeansOfType(Class<T> clas){
        return applicationContext.getBeansOfType(clas);
    }

}
