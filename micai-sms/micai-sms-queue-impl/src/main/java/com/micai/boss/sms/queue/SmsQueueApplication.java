package com.micai.boss.sms.queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.concurrent.CountDownLatch;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/24 12:07
 */
@SpringBootApplication
public class SmsQueueApplication implements CommandLineRunner, DisposableBean {

    private final Logger logger = LoggerFactory.getLogger(SmsQueueApplication.class);

    private final static CountDownLatch latch = new CountDownLatch(1);

    private static ConfigurableApplicationContext context;

    public static void main(String[] args) throws InterruptedException {
        context = SpringApplication.run(SmsQueueApplication.class, args);
        latch.await();
    }

    @Override
    public void run(String... args) throws Exception {
        logger.info("Sms queue------>>服务提供者启动完毕------>>启动完毕");
    }

    @Override
    public void destroy() throws Exception {
        latch.countDown();
        context.close();
        logger.info("Sms queue------>>服务提供者关闭------>>服务关闭");
    }
}
