package com.micai.boss.sms.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/24 14:18
 */
public class SmsTemplate implements Serializable {

    @NotNull
    private String name;

    @NotNull
    private String template;

    @NotNull
    private String providerName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }
}
