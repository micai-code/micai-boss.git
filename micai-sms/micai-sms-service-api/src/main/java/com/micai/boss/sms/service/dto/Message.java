package com.micai.boss.sms.service.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/24 14:15
 */
public class Message implements Serializable {

    @NotNull(message = "短信内容不能为空")
    private String content;

    @Pattern(regexp = "\\d{5,11}(,\\d{5,11})*")
    @NotNull(message = "收件人不能为空")
    private String recipients;

    @NotNull(message = "通道名称不能为空")
    private String providerName = "default";

    public Message() {

    }

    public Message(String recipients, String content){
        this.recipients = recipients;
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    @Override
    public String toString() {
        return "Message{" +
                "content='" + content + '\'' +
                ", recipients='" + recipients + '\'' +
                ", providerName='" + providerName + '\'' +
                '}';
    }
}
