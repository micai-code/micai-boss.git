package com.micai.boss.sms.constant;

/**
 * 描述：短信渠道
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/24 14:14
 */
public enum SmsChannelEnum {

    亿美("yimei","亿美");

    private String code;
    private String message;

    private SmsChannelEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static boolean contain(String value) {
        if (value == null) {
            return false;
        }
        SmsChannelEnum[] values = SmsChannelEnum.values();
        for (SmsChannelEnum sexEnum : values) {
            if (sexEnum.code.equals(value)) {
                return true;
            }
        }
        return false;
    }
}
