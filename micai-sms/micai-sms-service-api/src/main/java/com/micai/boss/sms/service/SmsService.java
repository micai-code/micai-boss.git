package com.micai.boss.sms.service;

import com.micai.boss.base.result.BaseResult;
import com.micai.boss.sms.service.dto.Message;

/**
 * 描述：短信服务
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/24 14:19
 */
public interface SmsService {

    /**
     * 发送短信
     * @param message
     * @return
     */
    public BaseResult<Boolean> send(Message message);

}
