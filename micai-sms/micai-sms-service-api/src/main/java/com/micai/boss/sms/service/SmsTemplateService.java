package com.micai.boss.sms.service;

import com.micai.boss.base.result.BaseResult;

import java.util.Map;

/**
 * 描述：短信业务模版,可以根据业务名称,生成短信模版,发送短信
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/24 14:20
 */
public interface SmsTemplateService {

    /**
     * 通过业务名称发送短信, recipients支持使用英文逗号分格批量发送短信
     *
     * @param code          模板名称
     * @param recipients    接收者
     * @param params        参数
     */
    BaseResult<Boolean> send(String code, String recipients, Map<String, String> params);
}
