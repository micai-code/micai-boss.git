package com.micai.boss.sms.provider;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/24 13:11
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Provider {

}
