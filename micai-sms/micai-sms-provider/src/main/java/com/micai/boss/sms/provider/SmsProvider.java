package com.micai.boss.sms.provider;

import com.micai.boss.sms.dto.Message;

/**
 * 描述：短信提供商
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/24 11:45
 */
public interface SmsProvider {

    /**
     * 是否可以发送这条短信
     * @param message
     * @return
     */
    Boolean isAvailable(Message message);

    /**
     * 发送Sms
     * @param message
     */
    Boolean send(Message message);
}
