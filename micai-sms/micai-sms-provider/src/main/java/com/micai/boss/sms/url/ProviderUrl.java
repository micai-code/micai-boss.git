package com.micai.boss.sms.url;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 描述：短信通道地址
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/24 13:14
 */
public class ProviderUrl {

    private static Logger logger = LoggerFactory.getLogger(ProviderUrl.class);

    private static Properties errorMsgProperties = new Properties();

    static {
        try (InputStream is = ProviderUrl.class.getResourceAsStream("provider.properties")) {
            errorMsgProperties.load(is);
        } catch (IOException e) {
            logger.error("读取短信错误信息映射文件出错", e);
        }
    }

    public static final String yimeiurl = errorMsgProperties.getProperty("micai.sms.yimei.url");

}
