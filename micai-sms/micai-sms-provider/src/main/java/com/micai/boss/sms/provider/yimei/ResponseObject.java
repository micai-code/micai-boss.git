package com.micai.boss.sms.provider.yimei;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/24 13:17
 */
public class ResponseObject {

    private static Logger logger = LoggerFactory.getLogger(ResponseObject.class);

    private String time;
    private String status;// 0表示发送成功
    private String msgId;// 返回的msgId，当发送成功时有值

    private static Properties errorMsgProperties = new Properties();

    static {
        try (InputStream is = ResponseObject.class.getResourceAsStream("error.properties")) {
            errorMsgProperties.load(is);
        } catch (IOException e) {
            logger.error("读取短信错误信息映射文件出错", e);
        }
    }

    public ResponseObject(String time, String status, String msgId){
        super();
        this.time = time;
        this.status = status;
        this.msgId = msgId;
    }

    public String getTime() {
        return time;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return status == null ? "" : errorMsgProperties.getProperty(status);
    }

    public String getMsgId() {
        return msgId;
    }

    public boolean isSuccess(){
        return "0".equals(status);
    }
}
