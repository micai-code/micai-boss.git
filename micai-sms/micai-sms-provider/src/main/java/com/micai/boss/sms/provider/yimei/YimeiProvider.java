package com.micai.boss.sms.provider.yimei;

import com.micai.boss.base.exception.ServiceException;
import com.micai.boss.sms.provider.Provider;
import com.micai.boss.sms.dto.Message;
import com.micai.boss.sms.provider.SmsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 描述：亿美软通短信通道
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/24 13:11
 */
@Provider
public class YimeiProvider implements SmsProvider {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private YimeiSMSInterfaceUtil yimeiChannel;

    @Override
    public Boolean isAvailable(Message message) {
        return "yimei".equals(message.getProviderName());
    }

    @Override
    public Boolean send(Message message) {
        ResponseObject r = yimeiChannel.sendBatchSignedMsg(message.getRecipients().split(","), message.getContent());
        this.logger.info("短信发送结果:" + r.getMessage());
        if (!"0".equals(r.getStatus())) {
            throw new ServiceException(r.getMessage());
        }
        return true;
    }
}
