package com.micai.boss.sms.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.micai.boss.sms.entity.SmsTemplateEntity;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/24 14:45
 */
public interface SmsTemplateMapper extends BaseMapper<SmsTemplateEntity> {

    SmsTemplateEntity findOneByCode(String code);
}
