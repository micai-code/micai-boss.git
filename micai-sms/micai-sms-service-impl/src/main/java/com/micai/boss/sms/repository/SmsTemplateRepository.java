package com.micai.boss.sms.repository;

import com.micai.boss.sms.entity.SmsTemplateEntity;
import com.micai.boss.sms.mapper.SmsTemplateMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/24 14:53
 */
@Repository
public class SmsTemplateRepository {

    @Autowired
    private SmsTemplateMapper smsTemplateMapper;

    public SmsTemplateEntity findOneByCode(String code){
        return smsTemplateMapper.findOneByCode(code);
    }
}
