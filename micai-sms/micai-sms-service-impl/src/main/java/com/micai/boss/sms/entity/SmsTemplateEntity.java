package com.micai.boss.sms.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.micai.boss.base.entity.BaseEntity;

/**
 * 描述：短信模板表
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/24 14:39
 */
@TableName("sms_template")
public class SmsTemplateEntity extends BaseEntity {

    @TableField("code")
    private String code;

    @TableField("template")
    private String template;

    @TableField("provider_name")
    private String providerName;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

}
