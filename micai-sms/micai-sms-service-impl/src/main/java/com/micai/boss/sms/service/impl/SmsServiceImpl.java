package com.micai.boss.sms.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.micai.boss.base.constant.ErrorCodeEnum;
import com.micai.boss.base.exception.ServiceException;
import com.micai.boss.base.result.BaseResult;
import com.micai.boss.sms.constant.SmsChannelEnum;
import com.micai.boss.sms.service.dto.Message;
import com.micai.boss.sms.queue.service.SmsTaskQueueService;
import com.micai.boss.sms.service.SmsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/24 14:25
 */
@Service(version = "1.0.0", protocol = {"dubbo", "feign"}, timeout = 10000)
public class SmsServiceImpl implements SmsService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SmsTaskQueueService smsTaskQueueService;

    /**
     * 发送SMS
     * @param message
     * @return
     */
    @Override
    public BaseResult<Boolean> send(Message message) {
        BaseResult<Boolean> result = new BaseResult<Boolean>(false);
        try {
            this.logger.info("发送短信:" + message.toString());
            if (message.getRecipients() != null) {
                message.setRecipients(message.getRecipients().replace(" ", ""));
            }
            boolean isSuccess = false;
            if(SmsChannelEnum.亿美.getCode().equals(message.getProviderName())){
                isSuccess = smsTaskQueueService.addTask(JSON.toJSONString(message));
            }
            result.setData(isSuccess);
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            result.setCode(e.getCode());
            result.setMessage(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            result.setCode(ErrorCodeEnum.系统异常.getCode());
            result.setMessage(ErrorCodeEnum.系统异常.getMessage());
        }
        return result;
    }
}
