package com.micai.boss.sms.queue.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.support.collections.DefaultRedisList;
import org.springframework.data.redis.support.collections.DefaultRedisMap;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.Assert;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/24 10:45
 */
public abstract class AbstractRedisTask<T> implements InitializingBean {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * 队列运行模式,默认为生产者
     */
    protected Boolean consumer = false;
    /**
     * 任务名称
     */
    protected String TASK_NAME = "redis-task-demo";
    /**
     * 空闲时，休息时间
     */
    protected static long SLEEP_TIME = 1000L;
    /**
     * 最大执行任务个数 阀值
     */
    protected static int TASK_FULL_SIZE = 50;
    /**
     * 判定任务 执行失败的时间间隔 阀值
     */
    protected static long INTERVAL_TIME = 3 * 60 * 1000L;
    /**
     * redis 任务队列zset key
     */
    protected String REDIS_TASK_LIST_KEY = "BAJIE_REDIS_TASK_LIST_USERID";
    /**
     * redis 正在执行中的map key
     */
    protected String REDIS_TASKING_SET_KEY = "BAJIE_REDIS_TASKING_SET_USERID";
    /**
     * 插入 redis任务队列失败 本地缓存队列
     */
    private ConcurrentLinkedQueue<String> bakTasks = new ConcurrentLinkedQueue<String>();
    /**
     * 处理任务 队列
     */
    private DefaultRedisList<String> taskQuene;
    /**
     * 处理中的任务  -- 处理完成remove，以确保任务不丢失，不重复
     */
    private DefaultRedisMap<String, Object> taskIngMap;
    /**
     * 队列实际业务类名称,内部类用$分割
     */
    private String EXECUTE_CLASS_NAME;
    protected Boolean RETRY_EXCEPTION = false;
    protected Boolean RETRY_FAIL = false;

    protected RedisTemplate redisTemplate;

    protected ThreadPoolTaskExecutor taskExecutor;

    /**
     * 添加待执行任务
     * @param taskId
     * @return
     */
    public Boolean addTask(String taskId) {
        Assert.notNull(taskId,"任务ID不能为空");
        try {
            if (taskQuene.contains(taskId)) {
                logger.info("[{} tasking add] task already in quene:{}", TASK_NAME, taskId);
                return false;
            }
            logger.info("[{} tasking add] add task:{}", TASK_NAME, taskId);
            return taskQuene.add(taskId);
        } catch (RedisConnectionFailureException ex) {
            logger.error("[{} tasking add] RedisConnectionFailureException  task:{}, error:{}.", TASK_NAME, taskId, ex);
            bakTasks.add(taskId);
        } catch (Exception e) {
            logger.error("[{} tasking add] task:{},error:{}", TASK_NAME, taskId, e);
            bakTasks.add(taskId);
        }
        return true;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        this.taskQuene = new DefaultRedisList<String>(this.REDIS_TASK_LIST_KEY, redisTemplate);
        this.taskIngMap = new DefaultRedisMap<String, Object>(this.REDIS_TASKING_SET_KEY, redisTemplate);
        // 判断是否为消费者
        if (consumer) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        try {
                            if (!taskQuene.isEmpty()) {
                                // 如果 当前任务个数超过阀值，跳过
                                if (taskExecutor.getThreadPoolExecutor().getQueue().size() >= TASK_FULL_SIZE) {
                                    Thread.sleep(SLEEP_TIME);
                                    continue;
                                }
                                // 从队列中获取任务ID 出队
                                String taskId = taskQuene.poll();
                                // 避免重复执行
                                if (taskIngMap.get(taskId) == null) {
                                    taskIngMap.put(taskId, taskId);
                                    taskExecutor.execute(new BaseTaskJobRunnable(taskId));
                                } else {
                                    logger.error("[{} thread task] already running job.taskId:{}", TASK_NAME, taskId);
                                }
                                continue;
                            }
                        } catch (RedisConnectionFailureException ex) {
                            logger.error("[{} thread task] async RedisConnectionFailureException error:{}.", TASK_NAME, ex);
                        } catch (Exception e) {
                            logger.error("[{} thread task] async system error:{}.", TASK_NAME, e);
                        }
                        try {
                            // 判断线程是否中断
                            if (Thread.interrupted()) {
                                logger.error("[{} thread task] async breaked InterruptedException.", TASK_NAME);
                                break;
                            }
                            Thread.sleep(SLEEP_TIME);
                        } catch (InterruptedException ex) {
                            logger.error("[{} thread task] async breaked InterruptedException:{}.", TASK_NAME, ex);
                            break;
                        }
                    }
                }
            }).start();
        }
        logger.info("[{} thread task] start suc. is {}", TASK_NAME, consumer ? "Consumer" : "Provider");
    }

    class BaseTaskJobRunnable implements Runnable {

        private Logger logger = LoggerFactory.getLogger(this.getClass());
        private String taskId;

        BaseTaskJobRunnable(String taskId) {
            this.taskId = taskId;
        }

        @Override
        public void run() {
            logger.info("[{} task] start.taskId:{}", TASK_NAME, taskId);
            long sTime = System.currentTimeMillis();
            try {
                // 开始任务执行
                TaskExecuteComponent taskExecuteComponent = (TaskExecuteComponent)Class.forName(EXECUTE_CLASS_NAME).newInstance();
                taskExecuteComponent.setTask(taskId);
                taskExecuteComponent.execute();
                taskIngMap.remove(taskId);
                logger.info("[{} task] end.context:{}.time:{}ms", TASK_NAME, taskId, System.currentTimeMillis() - sTime);
                return;
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                logger.error("[{} task] system error,context:{},error:{}", TASK_NAME, e);
            }catch (Exception e){
                e.printStackTrace();
            }
            // 异常是否重试
            if (RETRY_EXCEPTION){
                taskQuene.add(taskId);
            }
            taskIngMap.remove(taskId);
        }
    }

    public Boolean getConsumer() {
        return consumer;
    }

    public void setConsumer(Boolean consumer) {
        this.consumer = consumer;
    }

    public RedisTemplate getRedisTemplate() {
        return redisTemplate;
    }

    public void setRedisTemplate(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public ThreadPoolTaskExecutor getTaskExecutor() {
        return taskExecutor;
    }

    public void setTaskExecutor(ThreadPoolTaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    public String getTASK_NAME() {
        return TASK_NAME;
    }

    public void setTASK_NAME(String task_name) {
        this.TASK_NAME = task_name;
        this.REDIS_TASK_LIST_KEY = "BAJIE_REDIS_TASK_LIST_" + this.TASK_NAME.toUpperCase();
        this.REDIS_TASKING_SET_KEY = "BAJIE_REDIS_TASKING_SET_" + this.TASK_NAME.toUpperCase();
    }

    public String getEXECUTE_CLASS_NAME() {
        return EXECUTE_CLASS_NAME;
    }

    public void setEXECUTE_CLASS_NAME(String execute_class_name) {
        this.EXECUTE_CLASS_NAME = execute_class_name;
    }
}
