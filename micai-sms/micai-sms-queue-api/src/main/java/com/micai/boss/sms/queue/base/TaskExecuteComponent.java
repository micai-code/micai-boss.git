package com.micai.boss.sms.queue.base;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/24 10:44
 */
public interface TaskExecuteComponent {

    /**
     * 添加任务
     * @param task
     */
    void setTask(String task);

    /**
     * 实际业务执行,抛出异常为执行失败
     */
    void execute();
}
