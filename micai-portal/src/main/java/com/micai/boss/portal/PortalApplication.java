package com.micai.boss.portal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.util.concurrent.CountDownLatch;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/5/4 12:04
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.micai.boss"})
public class PortalApplication implements CommandLineRunner, DisposableBean {

    private final Logger log = LoggerFactory.getLogger(PortalApplication.class);

    private final static CountDownLatch latch = new CountDownLatch(1);

    private static ConfigurableApplicationContext context;

    public static void main(String [] args) throws InterruptedException {
        context = SpringApplication.run(PortalApplication.class, args);
        latch.await();
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Portal------>>服务启动完毕------>>启动完毕");
    }

    @Override
    public void destroy() throws Exception {
        latch.countDown();
        context.close();
        log.info("Portal------>>服务关闭------>>服务关闭");
    }
}
