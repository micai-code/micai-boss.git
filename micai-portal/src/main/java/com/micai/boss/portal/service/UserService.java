package com.micai.boss.portal.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.micai.boss.user.provider.UserProvider;
import org.springframework.stereotype.Component;
import java.util.Map;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/5/4 12:21
 */
@Component
public class UserService {

    @Reference(version = "1.0.0")
    private UserProvider userProvider;

    public Map<String,Object> getTaleData(int pageNum, int pageSize, String userName) {
        Map<String, Object> taleData = userProvider.getTaleData(pageNum, pageSize, userName);
        return taleData;
    }
}
