package com.micai.boss.portal.interceptor;

import com.micai.boss.base.interceptor.BaseInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/5/4 15:00
 */
@Configuration
public class PortalInterceptor extends BaseInterceptor {

    private static final Logger logger = LoggerFactory.getLogger(PortalInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        logger.info("portal拦截器调用");
        return super.preHandle(request, response, handler);
    }
}
