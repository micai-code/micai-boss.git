package com.micai.boss.portal.web;

import com.micai.boss.portal.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/5/4 12:01
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/getUserName")
    @ResponseBody
    public Map<String, Object> getUserName(String callack) {
        Map<String, Object> map = new HashMap<>();
        map.put("1", "111");
        map.put("2", "222");
        return map;
    }

    @GetMapping(value = "/index")
    public String index() {
        return "index";
    }

    @GetMapping(value = "/getTableData")
    @ResponseBody
    public Map<String, Object> getTableData(int pageNum, int pageSize, String userName) {
        return userService.getTaleData(pageNum, pageSize, userName);
    }

}
