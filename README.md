## 项目结构说明 ##
micai-boss  <br/>
-----micai-base      ----->基础模块             <br/>
-----micai-doc       ----->数据库脚本和文档      <br/>
-----micai-mongodb   ----->MongoDB服务插件      <br/>
-----micai-order     ----->订单模块             <br/>
-----micai-rabbitmq  ----->RabbitMQ服务插件     <br/>
-----micai-redis     ----->Redis服务插件        <br/>
-----micai-sms       ----->短信服务插件          <br/>
-----micai-thrid     ----->Dubbo服务插件        <br/>
-----micai-user      ----->用户服务             <br/>
-----micai-utils     ----->工具类模块           <br/>
-----micai-weixin    ----->微信API服务模块      <br/>

## 项目启动前准备 ##
1.安装MySql服务 <br/>
2.安装Zookeeper服务 <br/>
3.安装Redis服务 <br/>
4.安装RabbitMQ服务 <br/>
5.安装MongoDB服务 <br/>

## 项目启动步骤 ##
1.首先从git仓库下载项目到本地工程目录下 git clone https://gitee.com/micai/micai-boss.git <br/>
2.进入该项目的根目录下，通过命令的方式在本地编译项目 mvn clean compile -Dmaven.test.skip=true <br/>
3.编译成功后，导入IDEA工具中 <br/>
4.分别启动对应的Dubbo服务即可，这里一共有如下几个服务: <br/>
4.1.micai-user-provider，micai-user-consumer，micai-weixin-impl，micai-sms-queue-impl，micai-sms-service-impl，micai-order-consumer <br/>
4.2.启动其中的两个服务，其他的都是类似的启动方法，打开micai-user-provider，找到UserProviderApplication类启动即可 <br/>
4.3.当然启动Dubbo服务之前，一定记得先修改每个项目对应的配置文件，所有项目的配置文件都在resources目录下，读者可以根据自己实际情况修改即可 <br/>

## 2018-02-24 ##
技术选型：<br/>
1.Spring Boot <br/>
2.MyBatis <br/>
3.Dubbo <br/>
4.Zookeeper <br/>
5.MySql <br/>
6.Swagger2 <br/>
7.MongoDB <br/>
8.Redis <br/>
9.RabbitMQ <br/>

## 2018-04-19 ##
1.解决在分布式环境下的rabbitmq的使用 <br/>

## 2018-04-25 ##
1.解决分布式服务调用扫包问题 <br/>
2.集成最新版本的Swagger2 UI工具 <br/>

## 2018-04-26 ##
1.增加单例模式，抽象工厂模式，建造模式，原型模式实例代码 <br/>

## 2018-04-27 ##
1.增加分布式锁，基于redis实现 <br/>
2.增加分布式锁，基于zookeeper实现 <br/>

## 2018-04-28 ##
1.增加适配器模式实例代码 <br/>
2.增加责任链模式实例代码 <br/>
3.增加模板方法模式实例代码 <br/>

## 2018-04-29 ##
1.增加观察者模式实例代码 <br/>

## 2018-05-03 ##
1.添加了一些java集合操作类实例代码 <br/>

## 2018-05-21 ##
1.add single-sign-on-sso-with-json-web-token-jwt-spring-boot

## 建议及改进 ## <br/>
若您有任何建议，可以通过1）加入qq群715224124向群主提出，或2）发送邮件至827358369@qq.com向我反馈。本人承诺，任何<br/>
建议都将会被认真考虑，优秀的建议将会被采用，但不保证一定会在当前版本中实现。<br/>




