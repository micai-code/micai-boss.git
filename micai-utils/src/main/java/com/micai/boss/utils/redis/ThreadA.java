package com.micai.boss.utils.redis;

/**
 * 描述：模拟线程进行秒杀服务
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/27 11:07
 */
public class ThreadA extends Thread {

    private Service service;

    public ThreadA(Service service) {
        this.service = service;
    }

    @Override
    public void run() {
        service.seckill();
    }

//    public static void main(String [] args) {
//        Service service = new Service();
//        for (int i=0; i<100; i++) {
//            ThreadA threadA = new ThreadA(service);
//            threadA.start();
//        }
//    }
}
