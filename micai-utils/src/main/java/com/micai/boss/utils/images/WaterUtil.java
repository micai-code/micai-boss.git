package com.micai.boss.utils.images;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import org.apache.commons.lang3.StringUtils;

/**
 * @Author zhaoxinguo
 * @Date 2022-06-16 12:48
 * @ClassName WaterUtil
 * @Description JAVA添加文字/图片水印并设置水印位置和旋转角度
 */
public class WaterUtil {

    /**
     * 把水印印刷到图片上
     *
     * @param pressImg  -- 水印文件
     * @param targetImg -- 目标文件
     * @param newImg    -- 新文件
     * @param loaction  水印位置：left-top：左上角，right-top：右上角，left-bottom：左下角，right-bottom：右下角
     * @param degree    水印旋转角度
     */
    public static void pressImage(String pressImg, String targetImg, String newImg, String loaction, Integer degree) {
        try {
            // 目标文件
            File _file = new File(targetImg);
            Image src = ImageIO.read(_file);
            int wideth = src.getWidth(null);
            int height = src.getHeight(null);
            BufferedImage image = new BufferedImage(wideth, height, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();
            g.drawImage(src, 0, 0, wideth, height, null);


            // 水印文件
            File _filebiao = new File(pressImg);
            Image src_biao = ImageIO.read(_filebiao);
            int wideth_biao = src_biao.getWidth(null);
            int height_biao = src_biao.getHeight(null);
            // 水印坐标
            int x = 0, y = 0;
            if (StringUtils.equals(loaction, "left-top")) {
                g.drawImage(src_biao, x, y, wideth_biao, height_biao, null);
            } else if (StringUtils.equals(loaction, "right-top")) {
                x = wideth - wideth_biao;
            } else if (StringUtils.equals(loaction, "left-bottom")) {
                y = height - height_biao;
            } else if (StringUtils.equals(loaction, "right-bottom")) {
                x = wideth - wideth_biao;
                y = height - height_biao;
            } else {
                x = (wideth - wideth_biao) / 2;
                y = (height - height_biao) / 2;
            }

            if (null != degree) {
                // 设置水印旋转
                g.rotate(Math.toRadians(degree), x, y);
            }
            g.drawImage(src_biao, x, y, wideth_biao, height_biao, null);
            // 水印文件结束
            g.dispose();
            FileOutputStream out = new FileOutputStream(newImg);
            JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
            encoder.encode(image);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 打印文字水印图片
     *
     * @param fontStyle -- 字体
     * @param color     -- 字体颜色
     * @param fontSize  -- 字体大小
     * @param x         -- 偏移量x
     * @param y         -- 偏移量y
     * @param pressText --文字
     * @param targetImg -- 目标图片
     * @param fontName  -- 字体名
     */
    public static void pressText(String pressText, String targetImg, String fontName, int fontStyle, int color,
                                 int fontSize, int x, int y) {
        try {
            File _file = new File(targetImg);
            Image src = ImageIO.read(_file);
            int wideth = src.getWidth(null);
            int height = src.getHeight(null);
            BufferedImage image = new BufferedImage(wideth, height, BufferedImage.TYPE_INT_RGB);
            Graphics g = image.createGraphics();
            g.drawImage(src, 0, 0, wideth, height, null);

            g.setColor(Color.RED);
            g.setFont(new Font(fontName, fontStyle, fontSize));

            g.drawString(pressText, wideth - fontSize - x, height - fontSize / 2 - y);
            g.dispose();
            FileOutputStream out = new FileOutputStream(targetImg);
            JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
            encoder.encode(image);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public static void main(String[] args) {
//        File fileDir = new File("C:/Users/as/Desktop/img-test");
//        File[] tempList = fileDir.listFiles();
//        System.out.println("该目录下对象个数：" + tempList.length);
//        for (File file : tempList) {
//            if (file.isFile()) {
//                pressImage("C:/Users/as/Desktop/img-test/shuiyin.png", file.getPath(), file.getPath(), "right-top", 0);
//            }
//        }
//    }

    public static void main(String[] args) {
//        pressImage("C:/Users/as/Desktop/img-test/shuiyin.png", "C:/Users/as/Desktop/img-test/1.jpg", "C:/Users/as/Desktop/img-test/1.jpg", "left-top", -45);
//        pressImage("C:/Users/as/Desktop/img-test/shuiyin.png", "C:/Users/as/Desktop/img-test/2.jpg", "C:/Users/as/Desktop/img-test/2.jpg", "right-top", -45);
//        pressImage("C:/Users/as/Desktop/img-test/shuiyin.png", "C:/Users/as/Desktop/img-test/3.jpg", "C:/Users/as/Desktop/img-test/3.jpg", "center", -145);
//        pressImage("C:/Users/as/Desktop/img-test/shuiyin.png", "C:/Users/as/Desktop/img-test/4.jpg", "C:/Users/as/Desktop/img-test/4.jpg", "left-bottom", -45);
//        pressImage("C:/Users/as/Desktop/img-test/shuiyin.png", "C:/Users/as/Desktop/img-test/5.jpg", "C:/Users/as/Desktop/img-test/5.jpg", "right-bottom",
//                -45);
        pressImage("E:\\kizclub.com\\logo.png",
                "E:\\kizclub.com\\微信图片_20220608101656.png",
                "E:\\kizclub.com\\微信图片_20220608101656_水印.png",
                "right-bottom",
                0);
    }

}
