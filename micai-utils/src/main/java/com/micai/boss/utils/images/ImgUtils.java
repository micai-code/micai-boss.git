package com.micai.boss.utils.images;

import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.io.FileUtil;
import java.awt.*;
import java.io.File;

/**
 * @Author zhaoxinguo
 * @Date 2022-06-16 12:20
 * @ClassName ImgUtils
 * @Description TODO
 */
public class ImgUtils {

    public static void main(String[] args) {
        File srcFile = FileUtil.file("E:\\kizclub.com\\微信图片_20220119122621.jpg");
        File souFile = FileUtil.file("E:\\kizclub.com\\微信图片_20220119122621_水印.jpg");
        String text = "https://www.javaymw.com";
        ImgUtil.pressText(srcFile, souFile,
                text, Color.WHITE, //文字
                new Font("黑体", Font.BOLD, 24), //字体
                155, //x坐标修正值。 默认在中间，偏移量相对于中间偏移
                135, //y坐标修正值。 默认在中间，偏移量相对于中间偏移
                0.8f//透明度：alpha 必须是范围 [0.0, 1.0] 之内（包含边界值）的一个浮点数字
        );
    }
}
