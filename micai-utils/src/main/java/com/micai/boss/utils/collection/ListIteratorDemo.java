//package com.micai.boss.utils.collection;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.ListIterator;
//
///**
// * 描述：
// * <p>
// *
// * @author: 赵新国
// * @date: 2018/5/2 12:00
// */
//public class ListIteratorDemo {
//
//    public static void main(String [] args) {
//
//        List list = new ArrayList();
//        list.add("aaa");
//        list.add("bbb");
//        list.add("ccc");
//        list.add("ddd");
//        // next()
//        System.out.println("下标0开始：" + list.listIterator(0).next());
//        System.out.println("下标1开始：" + list.listIterator(1).next());
//        // 子列表
//        System.out.println("子List 1-3：" + list.subList(1, 3));
//
//        // 默认从下标0开始
//        ListIterator it = list.listIterator();
//        // 隐式光标属性add操作 ,插入到当前的下标的前面
//        it.add("sss");
//        while (it.hasNext()) {
//            System.out.println("next index=" + it.nextIndex() + ", Object=" + it.next());
//        }
//
//        // set属性
//        ListIterator it1 = list.listIterator();
//        it1.next();
//        it1.set("ooo");
//        // 下标
//        ListIterator it2 = list.listIterator(list.size());
//        while (it2.hasPrevious()) {
//            System.out.println("previous Index="+it2.previousIndex()+",Object="+it2.previous());
//        }
//    }
//}
