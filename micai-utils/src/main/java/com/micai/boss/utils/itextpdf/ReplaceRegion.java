package com.micai.boss.utils.itextpdf;

/**
 * @Author zhaoxinguo
 * @Date 2022-06-16 10:35
 * @ClassName ReplaceRegion
 * @Description 需要替换的区域
 */
public class ReplaceRegion {

    private String aliasName;
    private Float x;
    private Float y;
    private Float w;
    private Float h;

    public ReplaceRegion(String aliasName) {
        this.aliasName = aliasName;
    }

    /**
     * 替换区域的别名
     *
     * @return
     */
    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public Float getX() {
        return x;
    }

    public void setX(Float x) {
        this.x = x;
    }

    public Float getY() {
        return y;
    }

    public void setY(Float y) {
        this.y = y;
    }

    public Float getW() {
        return w;
    }

    public void setW(Float w) {
        this.w = w;
    }

    public Float getH() {
        return h;
    }

    public void setH(Float h) {
        this.h = h;
    }
}
