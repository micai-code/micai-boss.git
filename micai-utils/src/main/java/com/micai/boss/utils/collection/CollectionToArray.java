//package com.micai.boss.utils.collection;
//
//import java.util.ArrayList;
//import java.util.Collection;
//
///**
// * 描述：
// * <p>
// *
// * @author: 赵新国
// * @date: 2018/5/2 10:30
// */
//public class CollectionToArray {
//
//    public static void main(String [] args) {
//
//        // 创建一个集合对象
//        Collection collection1 = new ArrayList();
//        // 添加对象到Collection集合中
//        collection1.add("000");
//        collection1.add("111");
//        collection1.add("222");
//        System.out.println("集合collection1的大小：" + collection1.size());
//        System.out.println("集合collection1的内容：" + collection1);
//        // 从集合collection1中移除掉 "000" 这个对象
//        collection1.remove("000");
//        System.out.println("集合collection1移除 000 后的内容：" + collection1);
//        System.out.println("集合collection1中是否包含000：" + collection1.contains("000"));
//        System.out.println("集合collection1中是否包含111：" + collection1.contains("111"));
//
//        Collection collection2 = new ArrayList();
//        // 将collection1 集合中的元素全部都加到collection2中
//        collection2.addAll(collection1);
//        System.out.println("集合collection2的内容：" + collection2);
//        // 清空集合 collection1 中的元素
//        collection2.clear();
//        System.out.println("集合collection2是否为空："+collection2.isEmpty());
//
//        // 将集合collection1转化为数组
//        Object[] array = collection1.toArray();
//        for (int i=0; i<array.length; i++) {
//            System.out.println(array[i]);
//        }
//
//    }
//}
