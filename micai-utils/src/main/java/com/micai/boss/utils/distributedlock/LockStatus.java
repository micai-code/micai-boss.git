package com.micai.boss.utils.distributedlock;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/27 14:34
 */
public enum LockStatus {

    TRY_LOCK,
    LOCKED,
    UNLOCK;
}
