//package com.micai.boss.utils.collection;
//
//import java.util.ArrayList;
//import java.util.Collections;
//
///**
// * 描述：Sorting of ArrayList<Integer>
// * <p>
// *
// * @author: 赵新国
// * @date: 2018/5/3 17:55
// */
//public class ArrayListOfInteger {
//
//    public static void main(String [] args) {
//        ArrayList<Integer> arraylist = new ArrayList<Integer>();
//        arraylist.add(11);
//        arraylist.add(2);
//        arraylist.add(7);
//        arraylist.add(3);
//	    /* ArrayList before the sorting*/
//        System.out.println("Before Sorting:");
//        for(int counter: arraylist){
//            System.out.println(counter);
//        }
//
//        /* Sorting of arraylist using Collections.sort*/
//        Collections.sort(arraylist, Collections.reverseOrder());
//
//        /* ArrayList after sorting*/
//        System.out.println("After Sorting:");
//        for(int counter: arraylist){
//            System.out.println(counter);
//        }
//    }
//
//}
