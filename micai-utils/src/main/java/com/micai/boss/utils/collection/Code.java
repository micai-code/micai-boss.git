//package com.micai.boss.utils.collection;
//
///**
// * 描述：身份证类
// * <p>
// *
// * @author: 赵新国
// * @date: 2018/5/3 11:41
// */
//public class Code {
//
//    /**
//     * 身份证号码已经确认，不能改变
//     */
//    final int id;
//
//    Code(int i) {
//        id = i;
//    }
//
//    /**
//     * 身份号号码相同，则身份证相同
//     * @param object
//     * @return
//     */
//    @Override
//    public boolean equals(Object object) {
//        if (object instanceof Code) {
//            Code other = (Code) object;
//            return this.id == other.id;
//        }
//        return false;
//    }
//
//    /**
//     * 覆写hashCode方法，并使用身份证号作为hash值
//     * @return
//     */
//    @Override
//    public int hashCode() {
//        return id;
//    }
//
//    @Override
//    public String toString() {
//        return "身份证：" + id;
//    }
//
//}
