//package com.micai.boss.utils.collection;
//
//import java.util.HashMap;
//
///**
// * 描述：
// * <p>
// *
// * @author: 赵新国
// * @date: 2018/5/3 11:49
// */
//public class HashCodeEx {
//
//    public static void main(String [] args) {
//        HashMap map = new HashMap();
//        Person p1 = new Person("赵新国", new Code(123));
//        // 我们根据身份证来作为key值存放到Map中
//        map.put(p1.code, p1);
//        Person p2 = new Person("李思慧", new Code(456));
//        // 我们根据身份证来作为key值存放到Map中
//        map.put(p2.code, p2);
//        System.out.println("HashMap 中存放的人员信息:\n" + map);
//        // 赵新国改名为：赵心国但是还是同一个人。
//        Person p3 = new Person("赵心国", new Code(123));
//        map.put(p3.code, p3);
//        System.out.println("赵新国改名后 HashMap 中存放的人员信息:\n" + map);
//        // 查找身份证为：123 的人员信息
//        System.out.println("查找身份证为：123 的人员信息:" + map.get(new Code(123)));
//    }
//}
