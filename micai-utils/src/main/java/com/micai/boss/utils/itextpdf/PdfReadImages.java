package com.micai.boss.utils.itextpdf;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.text.PDFTextStripper;

/**
 * @author zhaoxinguo
 * @version 1.0.0
 * @project micai-boss
 * @description 读取PDF里面的图片资源
 * @date 2024/3/9 09:31:35
 */
public class PdfReadImages {

    public static void testPdf() {
        String path = "E:\\BaiduNetdiskDownload\\个人资料\\绘本故事\\卢森堡公园的一天.pdf";
        File file = new File(path);
        InputStream is = null;
        PDDocument document = null;
        int imageIndex = 1;
        try {
            if (path.endsWith(".pdf")) {
                document = PDDocument.load(file);
                document.getClass();
                //使用PDFTextStripper 工具
                PDFTextStripper tStripper = new PDFTextStripper();
                //设置文本排序，有规则输出
                tStripper.setSortByPosition(true);
                //获取所有文字信息
                String info = tStripper.getText(document);
                System.out.println("输出内容：" + info);
                int pageSize = document.getNumberOfPages();
                // 一页一页读取
                for (int i = 0; i < pageSize; i++) {
                    // 文本内容
                    PDFTextStripper stripper = new PDFTextStripper();
                    // 设置按顺序输出
                    stripper.setSortByPosition(true);
                    stripper.setStartPage(i + 1);
                    stripper.setEndPage(i + 1);
                    String text = stripper.getText(document);
                    System.out.println(text.trim());
                    System.out.println("-=-=-=-=-=-=-=-=-=-=-=-=-");

                    // 图片内容
                    PDPage page = document.getPage(i);
                    PDResources resources = page.getResources();
                    Iterable<COSName> cosNames = resources.getXObjectNames();
                    if (cosNames != null) {
                        Iterator<COSName> cosNamesIter = cosNames.iterator();
                        while (cosNamesIter.hasNext()) {
                            COSName cosName = cosNamesIter.next();
                            if (resources.isImageXObject(cosName)) {
                                PDImageXObject Ipdmage = (PDImageXObject) resources.getXObject(cosName);
                                BufferedImage image = Ipdmage.getImage();
                                FileOutputStream out = new FileOutputStream("E:\\BaiduNetdiskDownload\\个人资料\\绘本故事\\temp\\" + "卢森堡公园的一天_" +imageIndex + ".png");
                                imageIndex++;
                                try {
                                    ImageIO.write(image, "png", out);
                                } catch (IOException e) {
                                } finally {
                                    try {
                                        out.close();
                                    } catch (IOException e) {
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (InvalidPasswordException e) {
        } catch (IOException e) {
        } finally {
            try {
                if (document != null) {
                    document.close();
                }
                if (is != null) {
                    is.close();
                }
            } catch (IOException e) {
            }
        }
    }

    public static void main(String[] args) {
        testPdf();
    }
}
