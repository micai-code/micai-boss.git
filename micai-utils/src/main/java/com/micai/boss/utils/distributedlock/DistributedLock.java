package com.micai.boss.utils.distributedlock;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/27 14:34
 */
public interface DistributedLock {

    void lock() throws Exception;

    Boolean tryLock() throws Exception;

    Boolean tryLock(long millisecond) throws Exception;

    void unlock() throws Exception;
}
