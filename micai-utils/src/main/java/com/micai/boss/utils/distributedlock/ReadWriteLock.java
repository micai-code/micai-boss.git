package com.micai.boss.utils.distributedlock;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/27 14:43
 */
public interface ReadWriteLock {

    DistributedLock readLock();

    DistributedLock writeLock();
}
