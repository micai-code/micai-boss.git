package com.micai.boss.utils.itextpdf;

import com.itextpdf.awt.geom.Rectangle2D;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.parser.ImageRenderInfo;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.RenderListener;
import com.itextpdf.text.pdf.parser.TextRenderInfo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @Author zhaoxinguo
 * @Date 2022-06-16 10:47
 * @ClassName PdfConversion
 * @Description TODO
 */
public class PdfConversion {
    // 定义关键字
    private static String KEY_WORD = "by KIZCLUB.COM. All rights reserved.";
    // 定义返回值
    private static float[] resu = null;
    // 定义返回页码
    private static int i = 0;

    /*
     * 返回关键字所在的坐标和页数 float[0] >> X; float[1] >> Y; float[2] >> page;
     */

    public static float[] getKeyWords(String filePath) {
        try {
            PdfReader pdfReader = new PdfReader(filePath);
            int pageNum = pdfReader.getNumberOfPages();
            System.out.println(pageNum);
            PdfReaderContentParser pdfReaderContentParser = new PdfReaderContentParser(
                    pdfReader);

            // 下标从1开始
            for (i = 1; i <= pageNum; i++) {
                pdfReaderContentParser.processContent(i, new RenderListener() {

                    @Override
                    public void renderText(TextRenderInfo textRenderInfo) {
                        String text = textRenderInfo.getText();
                        if (null != text && text.contains(KEY_WORD)) {
                            Rectangle2D.Float boundingRectange = textRenderInfo.getBaseline().getBoundingRectange();
                            resu = new float[3];
                            System.out.println("=======" + text);
                            resu[0] = boundingRectange.x;
                            resu[1] = boundingRectange.y;
                            resu[2] = i;
                        }
                    }

                    @Override
                    public void renderImage(ImageRenderInfo arg0) {
                    }

                    @Override
                    public void endTextBlock() {

                    }

                    @Override
                    public void beginTextBlock() {
                    }
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resu;
    }

    public void manipulatePdf(String src, String dest) throws IOException, DocumentException {
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        float[] result = PdfConversion.getKeyWords(src); //1
        PdfContentByte canvas = stamper.getOverContent((int) result[2]);
//        float height=595;
//        System.out.println(canvas.getHorizontalScaling());
        float x,y;
        x= result[0];//2
        y = result[1];//3
        canvas.saveState();
        canvas.setColorFill(BaseColor.WHITE);
        canvas.rectangle(x, y, 40, 20);//设置覆盖面的大小

        canvas.fill();
        canvas.restoreState();
        //开始写入文本
        canvas.beginText();
        //BaseFont bf = BaseFont.createFont(URLDecoder.decode(CutAndPaste.class.getResource("/AdobeSongStd-Light.otf").getFile()), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
//        BaseFont bf = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.EMBEDDED);
        BaseFont bf = BaseFont.createFont("C:/WINDOWS/Fonts/STSONG.TTF", BaseFont.IDENTITY_H,BaseFont.NOT_EMBEDDED);  //使用Windows系统字体(TrueType)
        Font font = new Font(bf,10,Font.BOLD);
        //设置字体和大小
        canvas.setFontAndSize(font.getBaseFont(), 15);
        //设置字体的输出位置
        canvas.setTextMatrix(x, y-1);
        //要输出的text
        canvas.showText("by JAVAYMW.COM. All rights reserved.");

        //设置字体的输出位置
//        canvas.setFontAndSize(font.getBaseFont(), 20);
////        canvas.setTextMatrix(x, y-90);
////        //要输出的text
////        canvas.showText("多退少补");

        canvas.endText();
        stamper.close();
        reader.close();
        System.out.println("complete");
    }

    public static final String SRC = "E:\\kizclub.com\\fruits(C) - 副本.pdf";
    public static final String DEST = "E:\\kizclub.com\\fruits(C) - 副本-2.pdf";
    public static void main(String[] args) throws IOException, DocumentException {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        new PdfConversion().manipulatePdf(SRC, DEST);
    }
}
