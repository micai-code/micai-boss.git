//package com.micai.boss.utils.collection;
//
//import java.util.HashMap;
//import java.util.LinkedHashMap;
//import java.util.Map;
//import java.util.TreeMap;
//
///**
// * 描述：排序的Map
// * <p>
// *
// * @author: 赵新国
// * @date: 2018/5/2 15:36
// */
//public class MapSortDemo {
//
//    public static void main(String [] args) {
//
//        Map map1 = new HashMap();
//        Map map2 = new LinkedHashMap();
//        for (int i=0; i<10; i++) {
//            // 产生一个随机数，并将其放入Map中
//            double s = Math.random()*100;
//            map1.put(new Integer((int)s), "第"+i+"个放入的元素：" + s+"\n");
//            map2.put(new Integer((int)s), "第"+i+"个放入的元素：" + s+"\n");
//        }
//        System.out.println("未排序前HashMap："+map1);
//        System.out.println("未排序前LinkedHashMap："+map2);
//
//        // 使用TreeMap来对另外的Map进行重构和排序
//        Map sortedMap = new TreeMap(map1);
//        System.out.println("排序后：" + sortedMap);
//        System.out.println("排序后：" + new TreeMap(map2));
//    }
//}
