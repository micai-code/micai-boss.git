//package com.micai.boss.utils.collection;
//
///**
// * 描述：人员信息类
// * <p>
// *
// * @author: 赵新国
// * @date: 2018/5/3 11:44
// */
//public class Person {
//
//    Code code;//身份证
//    String name;//姓名
//
//    public Person(String name, Code code) {
//        this.name = name;
//        this.code = code;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        if (object instanceof Person) {
//            Person other = (Person) object;
//            return this.code.equals(other.code);
//        }
//        return false;
//    }
//
//    @Override
//    public String toString() {
//        return "姓名：" + name + "身份证：" + code.id + "\n";
//    }
//
//}
