//package com.micai.boss.utils.collection;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.LinkedList;
//import java.util.List;
//
///**
// * 描述：
// * <p>
// *
// * @author: 赵新国
// * @date: 2018/5/2 11:48
// */
//public class ListDemo {
//
//    public static void main(String [] args) {
//
//        /*LinkedList queue = new LinkedList();
//        queue.addFirst("Bernadine");
//        queue.addFirst("Elizabeth");
//        queue.addFirst("Gene");
//        queue.addFirst("Elizabeth");
//        queue.addFirst("Clara");
//        System.out.println(queue);
//        queue.removeLast();
//        queue.removeLast();
//        System.out.println(queue);*/
//
//        /*List list = new LinkedList();
//        list.add("2018-04-21");
//        list.add("2018-04-22");
//        list.add("2018-04-23");
//        list.add("2018-04-24");
//        list.add("2018-04-25");
//        list.add("2018-04-26");
//        list.add("2018-04-27");
//        list.add("2018-04-28");
//        list.add("2018-04-29");
//        list.add("2018-04-30");
//        list.add("2018-05-01");
//        list.add("2018-05-02");
//        System.out.println(list.size());
//        System.out.println(list.subList(7, 12));*/
//
//        List<Integer> list = new ArrayList<>();
//        list.add(12);
//        list.add(43);
//        list.add(1);
//        list.add(56);
//        list.add(23);
//        Collections.sort(list);
//        System.out.println(list);
//
//    }
//}
