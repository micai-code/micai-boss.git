//package com.micai.boss.utils.collection;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.Iterator;
//
///**
// * 描述：迭代器的简单使用
// * <p>
// *
// * @author: 赵新国
// * @date: 2018/5/2 11:08
// */
//public class IteratorDemo {
//
//    public static void main(String [] args) {
//
//        Collection collection = new ArrayList();
//        collection.add("s1");
//        collection.add("s2");
//        collection.add("s3");
//        // 得到一个迭代器
//        Iterator iterator = collection.iterator();
//        // 遍历
//        while (iterator.hasNext()) {
//            Object obj = iterator.next();
//            System.out.println("iterator=" + obj);
//        }
//        if (collection.isEmpty()) {
//            System.out.println("collection is Empty!");
//        } else {
//            System.out.println("collection is not Empty! size=" + collection.size());
//        }
//
//        Iterator iterator2 = collection.iterator();
//        // 移除元素
//        while (iterator2.hasNext()) {
//            Object obj = iterator2.next();
//            System.out.println("remove: " + obj);
//            iterator2.remove();
//        }
//
//        Iterator iterator3 = collection.iterator();
//        // 查看是否还有元素
//        if (!iterator3.hasNext()) {
//            System.out.println("还有元素");
//        }
//
//        // 使用collection.isEmpty()方法来判断
//        if (collection.isEmpty()) {
//            System.out.println("collection is Empty!");
//        }
//    }
//}
