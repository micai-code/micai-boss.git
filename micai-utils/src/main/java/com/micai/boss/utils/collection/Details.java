//package com.micai.boss.utils.collection;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.List;
//
///**
// * 描述：Sorting of ArrayList<String>
// * <p>
// *
// * @author: 赵新国
// * @date: 2018/5/3 17:52
// */
//public class Details {
//
//    public static void main(String [] args) {
//        List<String> listofcountries = new ArrayList();
//        listofcountries.add("India");
//        listofcountries.add("US");
//        listofcountries.add("China");
//        listofcountries.add("Denmark");
//
//        /*Unsorted List*/
//        System.out.println("Before Sorting:");
//        for (String counter : listofcountries) {
//            System.out.println(counter);
//        }
//
//        /* Sort statement*/
//        //Collections.sort(listofcountries); // ASC
//        Collections.sort(listofcountries, Collections.reverseOrder());// DESC
//
//        /* Sorted List*/
//        System.out.println("After Sorting:");
//        for(String counter: listofcountries){
//            System.out.println(counter);
//        }
//    }
//
//}
