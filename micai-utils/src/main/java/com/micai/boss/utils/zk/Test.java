package com.micai.boss.utils.zk;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/4/27 12:11
 */
public class Test {

    static int n = 500;

    /**
     * 模拟秒杀请求，扣减库存
     */
    public static void secskill() {
        System.out.println(--n);
    }

//    public static void main(String[] args) {
//
//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                DistributedLockZk lock = null;
//                try {
//                    lock = new DistributedLockZk("127.0.0.1:2181", "test1");
//                    lock.lock();
//                    secskill();
//                    System.out.println(Thread.currentThread().getName() + "正在运行");
//                } catch (Exception e) {
//                    e.printStackTrace();
//                } finally {
//                    if (lock != null) {
//                        lock.unlock();
//                    }
//                }
//            }
//        };
//
//        for (int i = 0; i < 10; i++) {
//            Thread t = new Thread(runnable);
//            t.start();
//        }
//    }
}
